<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Plugin\Backend\Block\Dashboard;

class Grids
{
    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    private $authorization;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     */
    public function __construct(
        \Magento\Framework\AuthorizationInterface $authorization,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        $this->authorization = $authorization;
        $this->user = $user;
    }

    /**
     * @return \Magento\Backend\Block\Dashboard\Grids
     * @throws \Exception
     */
    public function afterSetLayout(\Magento\Backend\Block\Dashboard\Grids $subject, $result)
    {

        if ($this->authorization->isAllowed('Celesta_AdvancedGoogleCalendar::dashboard')) {
            foreach ($this->user->getDashboardCalendars() as $calendar) {
                $subject->addTab(
                    'celesta_calendar' . $calendar->getId(),
                    [
                        'label' => $calendar->getSummary(),
                        'content' => $subject->getLayout()->createBlock(
                            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\Dashboard\Calendar::class,
                            'db_calendar_block' . $calendar->getId(),
                            ['calendar' => $calendar]
                        )->toHtml()
                    ]
                );
            }
        }

        return $result;
    }
}
