<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Plugin\User\Controller\Adminhtml\User;

class Save
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler
     */
    private $exceptionHandler;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\UserFactory
     */
    private $userFactory;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $cacheTypeList;

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler
     * @param \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler,
        \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
    ) {
        $this->request = $request;
        $this->exceptionHandler = $exceptionHandler;
        $this->userFactory = $userFactory;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
    }

    /**
     * @return void
     */
    public function afterExecute()
    {
        try {
            $userId = $this->request->getParam('user_id', null);
            if ($userId === null) {
                return;
            }
            $clientId = trim($this->request->getParam('client_id'));
            $clientSecret = trim($this->request->getParam('client_secret'));

            /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
            $calendarUser = $this->userFactory->create(['userId' => $userId]);
            if (!empty($clientId)) {
                $calendarUser->saveClientId($clientId);
            }
            if (!empty($clientSecret)) {
                $calendarUser->saveClientSecret($clientSecret);
            }
            if (!empty($clientId) && !empty($clientSecret)) {
                $key = \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH . '/user_ids';
                $calendarUsers = $this->scopeConfig->getValue($key);
                $calendarUsers = $calendarUsers ? explode(',', $calendarUsers) : [];
                if (!in_array($userId, $calendarUsers, false)) {
                    $calendarUsers[] = $userId;
                    $calendarUsers = array_unique($calendarUsers);
                    sort($calendarUsers);
                    $this->configWriter->save($key, implode(',', $calendarUsers));
                    $this->cacheTypeList->cleanType(\Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER);
                }
            }
        } catch (\Exception $e) {
            $this->exceptionHandler->handle($e);
        }
    }
}
