<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Event;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;
use Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar;
use Celesta\AdvancedGoogleCalendar\Model\ReminderHelper;
use Celesta\AdvancedGoogleCalendar\Model\TagRepository;

/**
 * Class Save
 */
class Save extends Manage
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    private $timezone;

    /**
     * @var GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var ReminderHelper
     */
    private $reminderHelper;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper
     */
    private $eventMapper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ReminderPersistence
     */
    private $reminderPersistence;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param GoogleCalendar $googleCalendar
     * @param ReminderHelper $reminderHelper
     * @param TagRepository $tagRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        GoogleCalendar $googleCalendar,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        ReminderHelper $reminderHelper,
        TagRepository $tagRepository,
        \Celesta\AdvancedGoogleCalendar\Model\ReminderPersistence $reminderPersistence,
        \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper
    ) {
        parent::__construct($context);
        $this->googleCalendar = $googleCalendar;
        $this->reminderHelper = $reminderHelper;
        $this->timezone = $timezone;
        $this->tagRepository = $tagRepository;
        $this->eventMapper = $eventMapper;
        $this->reminderPersistence = $reminderPersistence;
        $this->user = $user;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @inheritdoc
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->_formKeyValidator->validate($this->getRequest())) {
            try {
                if ($this->getEventId()) {
                    $calendar = $this->user->getUserCalendarById($this->getCalendarId());
                } else {
                    $params = $this->getRequest()->getParam('general', []);
                    $calendar = $this->user->getUserCalendarById($params['calendar_id']);
                }
                $googleEvent = $this->getGoogleCalendarEvent();
                $event = $this->googleCalendar->saveEvent($googleEvent, $calendar->getGoogleId());
                $eventId = $event->getId();
                if ($eventId) {
                    $redirectParams = $this->getRedirectParams($eventId, $calendar->getId());
                    $this->messageManager->addSuccessMessage(__('You saved the event.'));

                    return $resultRedirect->setPath(...$redirectParams);
                }
                $this->messageManager->addErrorMessage('No data saved');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('Something went wrong.'));
            }
        }
        return $resultRedirect->setRefererUrl();
    }

    /**
     * @return \Google_Service_Calendar_Event
     */
    private function getGoogleCalendarEvent()
    {
        if ($this->getEventId() && $this->getCalendarId()) {
            $calendar = $this->user->getUserCalendarById($this->getCalendarId());
            $googleEvent = $this->googleCalendar->getEventById($calendar->getGoogleId(), $this->getEventId());
        } else {
            $googleEvent = new \Google_Service_Calendar_Event();
        }
        $googleEvent->setSummary($this->getSummary());
        $googleEvent->setLocation($this->getLocation());
        $googleEvent->setDescription($this->getDescription());
        $googleEvent->setColorId($this->getColorId());
        $googleEvent->setVisibility($this->getVisibility());
        $googleEvent->setTransparency($this->getTransparency());

        if ($this->getRecurrence() === false) {
            $getDate = function (\Google_Service_Calendar_EventDateTime $eventDate, $date) {
                if (strpos($date, 'T') === false) {
                    $eventDate->setDate($date);
                } else {
                    $eventDate->setDateTime($date);
                }
                return $eventDate;
            };
            $googleEvent->setStart($getDate(new \Google_Service_Calendar_EventDateTime(), $this->getStart()));
            $googleEvent->setEnd($getDate(new \Google_Service_Calendar_EventDateTime(), $this->getEnd()));
        }

        /** Guests */
        $googleEvent->setAttendees($this->getAttendees());
        $googleEvent->setGuestsCanModify($this->getGuestsCanModify());
        $googleEvent->setGuestsCanInviteOthers($this->getGuestsCanInviteOthers());
        $googleEvent->setGuestsCanSeeOtherGuests($this->getGuestsCanSeeOtherGuests());
        /** [END]Guests */

        /** Reminders */
        $reminders = $this->reminderHelper->normalize($this->getReminders());
        $googleReminders = $this->reminderHelper->getGoogleReminders($reminders);
        $eventReminders = new \Google_Service_Calendar_EventReminders();
        if ($googleReminders) {
            $calendarReminders = [];
            foreach ($googleReminders as $data) {
                $reminder = new \Google_Service_Calendar_EventReminder();
                $reminder->setMethod($data['type']);
                $reminder->setMinutes($data['before_minutes']);
                $calendarReminders[] = $reminder;
            }
            $eventReminders->setOverrides($calendarReminders);
            $googleEvent->setReminders($eventReminders);
        }
        $properties = [];
        $magentoReminders = $this->reminderHelper->getSystemReminders($reminders);
        if ($magentoReminders) {
            $magentoReminder = reset($magentoReminders);
            $properties[ReminderHelper::GOOGLE_CALENDAR_SYSTEM_KEY] = $magentoReminder['before_minutes'];
        }
        if ($magentoReminders || $googleReminders) {
            $eventReminders->setUseDefault(false);
        } else {
            $eventReminders->setUseDefault(true);
        }
        $googleEvent->setReminders($eventReminders);
        /** [END]Reminders */

        /** Tags */
        $tagTitles = $this->tagRepository->getTitlesByIds($this->getTagIds());
        if ($tagTitles) {
            $properties['tags'] = implode(',', $tagTitles);
        }
        /** [END]Tags */

        $extendedProperties = new \Google_Service_Calendar_EventExtendedProperties();
        $extendedProperties->setShared($properties);
        $googleEvent->setExtendedProperties($extendedProperties);

        return $googleEvent;
    }

    private function getCalendarId()
    {
        return $this->getRequest()->getParam('calendar_id');
    }

    private function getEventId()
    {
        return $this->getRequest()->getParam('event_id');
    }

    private function getSummary()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['summary']) ? null : $params['summary'];
    }

    private function getDescription()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['description']) ? null : $params['description'];
    }

    private function getLocation()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['location']) ? null : $params['location'];
    }

    private function getColorId()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['color_id']) ? null : (int)$params['color_id'];
    }

    private function getRecurrence()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['recurrence']) ? false : (bool)$params['recurrence'];
    }

    private function getVisibility()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['visibility']) ?
            'default' : $this->eventMapper->getVisibilityGoogle($params['visibility']);
    }

    private function getTransparency()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['transparency']) ?
            'opaque' : $this->eventMapper->getTransparencyGoogle($params['transparency']);
    }

    private function getStart()
    {
        $params = $this->getRequest()->getParam('general', []);
        $eventDateTime = $params['event_date_time'];

        return $this->normalizeDateTime($eventDateTime['date_from'], $eventDateTime['time_from']);
    }

    private function getEnd()
    {
        $params = $this->getRequest()->getParam('general', []);
        $eventDateTime = $params['event_date_time'];
        return $this->normalizeDateTime($eventDateTime['date_to'], $eventDateTime['time_to'], '+1 days');
    }

    private function normalizeDateTime($date, $time, $modify = null)
    {
        $params = $this->getRequest()->getParam('general', []);
        $eventDateTime = $params['event_date_time'];
        $date = new \DateTimeImmutable($date, new \DateTimeZone($this->timezone->getConfigTimezone()));
        if ((bool)$eventDateTime['all_day']) {
            return $modify ? $date->modify($modify)->format('Y-m-d') : $date->format('Y-m-d');
        }
        if (strpos($time, 'T') !== false) {
            preg_match('/^.+T(\d\d:\d\d).+$/', $time, $matches);
            $time = end($matches);
        }

        return preg_replace('/^(.+T)(\d\d:\d\d)(.+)$/', '${1}' . $time . '$3', $date->format(\DateTime::ATOM));
    }

    /**
     * @return \Google_Service_Calendar_EventAttendee[]
     */
    private function getAttendees()
    {
        $guests = $this->getRequest()->getParam('guests', []);
        $attendees = [];
        if (!empty($guests['guest_emails'])) {
            $emails = explode(',', $guests['guest_emails']);
            foreach ($emails as $email) {
                $attendee = new \Google_Service_Calendar_EventAttendee();
                $email = trim($email);
                preg_match('/^([^<>]+) ?<(.+@.+\..+)>$/', $email, $matches);
                $emailMatch = null;
                if (count($matches) === 3) {
                    $nameMatch = trim($matches[1]);
                    $emailMatch = trim($matches[2]);
                    $attendee->setDisplayName($nameMatch);
                } elseif (preg_match('/^.+@.+\..+$/', $email)) {
                    $emailMatch = $email;
                }
                if ($emailMatch) {
                    $attendee->setEmail($emailMatch);
                    $attendees[] = $attendee;
                }
            }
        }
        return $attendees;
    }

    private function getGuestsCanModify()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_modify_event']) ? false : (bool)$guests['guests_modify_event'];
    }

    private function getGuestsCanInviteOthers()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_invite_others']) ? false : (bool)$guests['guests_invite_others'];
    }

    private function getGuestsCanSeeOtherGuests()
    {
        $guests = $this->getRequest()->getParam('guests', []);

        return empty($guests['guests_see_guest_list']) ? false : (bool)$guests['guests_see_guest_list'];
    }

    /**
     * @return array
     */
    protected function getReminders()
    {
        $reminderSettings = $this->getRequest()->getParam('reminders', []);

        return isset($reminderSettings['reminders']) ? $reminderSettings['reminders'] : [];
    }

    /**
     * @return array
     */
    protected function getTagIds()
    {
        $tagIds = [];
        $mainData = $this->getRequest()->getParam('general', []);
        if (!empty($mainData['data']['tags'])) {
            $tagIds = array_map('intval', $mainData['data']['tags']);
            $tagIds = array_filter($tagIds);
        }

        return $tagIds;
    }

    /**
     * @param string $eventId
     * @param int $calendarId
     * @return array
     */
    protected function getRedirectParams($eventId, $calendarId)
    {
        $alias = $this->getRequest()->getParam('back_alias');
        switch ($alias) {
            case 'new':
                return ['*/*/edit'];
            case 'close':
                return ['*/*/'];
            default:
                return ['*/*/edit', ['calendar' => $calendarId, 'event' => $eventId]];
        }
    }
}
