<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Event;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class MassDelete extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventRepository
     */
    private $eventRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $calendar;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventRepository $eventRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\EventRepository $eventRepository,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
    ) {
        parent::__construct($context);
        $this->user = $user;
        $this->eventRepository = $eventRepository;
        $this->calendar = $calendar;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function execute()
    {
        $eventHashes = $this->getRequest()->getParam('selected');
        if ($eventHashes) {
            $events = $this->eventRepository->selectAllWhere(['event_hash IN (?)' => $eventHashes]);
            $calendarEvents = [];
            foreach ($events as $event) {
                $calendarEvents[$event['calendar_id']][] = $event;
            }

            $calendarIds = array_keys($calendarEvents);
            $countCalendars = 0;
            $countEvents = 0;
            foreach ($calendarIds as $calendarId) {
                $calendar = $this->user->getUserCalendarById($calendarId);
                if ($calendar) {
                    $eventIds = array_column($calendarEvents[$calendarId], 'event_id');
                    $this->calendar->deleteEventsById($calendar->getGoogleId(), $eventIds);
                    $countEvents += count($eventIds);
                }
                $countCalendars++;
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 event(s) in %2 calendar(s) have been deleted.', $countEvents, $countCalendars));
        }

        return $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT)
            ->setPath(\Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'event');
    }
}
