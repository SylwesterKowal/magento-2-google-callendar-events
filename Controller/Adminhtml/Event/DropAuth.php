<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Event;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class DropAuth extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\UserFactory
     */
    private $userFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory
    ) {
        parent::__construct($context);
        $this->userFactory = $userFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \Magento\Framework\Exception\InputException
     * @inheritdoc
     */
    public function execute()
    {
        $userId = $this->getRequest()->getParam('user_id');
        if ($userId) {
            /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
            $calendarUser = $this->userFactory->create(['userId' => $userId]);
            $calendarUser->dropAuth();
            $calendarUser->dropAccessToken();
            $this->messageManager->addSuccessMessage(
                __('Google authentication data for Celesta extensions was successfully dropped.')
            );
            $resultRedirect = $this->resultRedirectFactory->create();

            return $resultRedirect->setPath(
                'adminhtml/user/edit/user_id/' . $calendarUser->getId(),
                ['active_tab' => 'celesta_settings']
            );
        }
        throw new \Magento\Framework\Exception\InputException();
    }
}
