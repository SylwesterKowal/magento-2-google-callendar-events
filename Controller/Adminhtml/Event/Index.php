<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Event;

class Index extends \Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler
     */
    private $exceptionHandler;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $calendar;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        parent::__construct($context);
        $this->exceptionHandler = $exceptionHandler;
        $this->user = $user;
        $this->calendar = $calendar;
        $this->objectManager = $objectManager;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \InvalidArgumentException
     */
    public function execute()
    {
        try {
            $this->calendar->syncCalendarsAndEvents();
        } catch (\Celesta\AdvancedGoogleCalendar\Model\Exception $e) {
            if ($e->getCode() ==
                \Celesta\AdvancedGoogleCalendar\Model\Exception::REASON_GOOGLE_ACCESS_TOKEN_IS_MISSING) {
                return $this->resultRedirectFactory->create()->setUrl($this->calendar->getAuthUrl());
            }
            $this->messageManager->addErrorMessage($e->getMessage());

            return $this->resultRedirectFactory->create()->setUrl($this->user->getUserSettingsUrl());
        } catch (\Exception $e) {
            $this->exceptionHandler->handle($e);
            if ($e instanceof \Google_Service_Exception) {
                return $this->resultRedirectFactory->create()->setUrl($this->user->getUserSettingsUrl());
            }
        }
        /** @var \Magento\Framework\View\Result\Layout $resultLayout */
        $resultLayout = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $resultLayout->getConfig()->getTitle()->prepend(__('Calendar Events'));

        return $resultLayout;
    }
}
