<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Event;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;
use Magento\Framework\Controller\ResultFactory;

class Calendar extends Manage
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // https://fullcalendar.io/
        $contents = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.0/fullcalendar.min.css" />
        <script
          src="https://code.jquery.com/jquery-2.2.4.min.js"
          integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
          crossorigin="anonymous"></script>
        <script src=\'https://momentjs.com/downloads/moment.min.js\'></script>
        <script src=\'https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.5.0/fullcalendar.min.js\'></script>
        <script src=\'http://magento2ce.dev/custom/gcal.js\'></script>

        <script type=\'text/javascript\'>
                $(document).ready(function() {
                    $(\'#calendar\').fullCalendar({
                googleCalendarApiKey: \'AIzaSyCvR93XZFaD7k7nApxYBR5qSp4IS3dZJVs\',
                events: {
                        googleCalendarId: \'5e5ob12tmgd103801srarqgsfc@group.calendar.google.com\'
                }
            });
        });
        </script>
        <div id=\'calendar\'></div>';

        /** @var \Magento\Framework\View\Result\Layout $resultLayout */
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultRaw->setContents($contents);
        return $resultRaw;
    }
}
