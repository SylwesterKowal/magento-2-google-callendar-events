<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Calendar;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class Save extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository
     */
    private $calendarRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler
     */
    private $exceptionHandler;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository,
        \Celesta\AdvancedGoogleCalendar\Model\ExceptionHandler $exceptionHandler,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        parent::__construct($context);
        $this->googleCalendar = $googleCalendar;
        $this->calendarRepository = $calendarRepository;
        $this->exceptionHandler = $exceptionHandler;
        $this->user = $user;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->_formKeyValidator->validate($this->getRequest())) {
            try {
                $googleId = $this->getCalendarGoogleId();
                $calendarListEntry = $updatedCalendar = $this->googleCalendar->getCalendarById($googleId);

                $calendarListEntry->setSummary($this->getSummary());
                $calendarListEntry->setDescription($this->getDescription());
                $useSystemTimeZone = $this->getUseSystemTimeZone();
                if (!$useSystemTimeZone) {
                    $calendarListEntry->setTimeZone($this->getTimeZone());
                }
                if (!preg_match('/.*#(holiday|contacts)@group\.v\.calendar\.google\.com/', $googleId)) {
                    $updatedCalendarListEntry = $this->googleCalendar->updateCalendarById($googleId, $calendarListEntry);
                }
                $this->updateModel($updatedCalendarListEntry);

                $this->messageManager->addSuccessMessage(__('Calendar saved.'));
            } catch (\Exception $e) {
                $this->exceptionHandler->handle($e);
            }
        }

        return $resultRedirect->setRefererUrl();
    }

    private function getCalendarId()
    {
        return $this->getRequest()->getParam('calendar_id');
    }

    private function getCalendarGoogleId()
    {
        return $this->getRequest()->getParam('calendar_google_id');
    }

    private function getSummary()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['summary']) ? null : $params['summary'];
    }

    private function getDescription()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['description']) ? null : $params['description'];
    }

    private function getTimeZone()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['time_zone']) ? null : $params['time_zone'];
    }

    private function getUseSystemTimeZone()
    {
        $params = $this->getRequest()->getParam('general', []);

        return empty($params['use_system_time_zone']) ? null : $params['use_system_time_zone'];
    }

    private function getIsConnected()
    {
        $params = $this->getRequest()->getParam('magento', []);

        return empty($params['is_connected']) ? 0 : (int)$params['is_connected'];
    }

    private function getIsDashboard()
    {
        $params = $this->getRequest()->getParam('magento', []);

        return empty($params['is_dashboard']) ? 0 : (int)$params['is_dashboard'];
    }

    private function getShareReminders()
    {
        $params = $this->getRequest()->getParam('magento', []);

        return empty($params['share_reminders']) ? 0 : (int)$params['share_reminders'];
    }

    private function updateModel(\Google_Service_Calendar_CalendarListEntry $googleCalendar)
    {
        $data = $this->calendarRepository->getRowById($this->getCalendarId());
        $model = $this->calendarRepository->getPopulated($data);
        $model->setGoogleId($googleCalendar->getId());
        $model->setSummary($googleCalendar->getSummary());
        $model->setDescription($googleCalendar->getDescription());
        $model->setUseSystemTimeZone($this->getUseSystemTimeZone());
        $model->setTimeZone($googleCalendar->getTimeZone());
        $model->setIsConnected($this->getIsConnected());
        $model->setIsDashboard($this->getIsDashboard());
        $model->setShareReminders($this->getShareReminders());
        $this->calendarRepository->save($model);
    }
}
