<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Calendar;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;
use Magento\Framework\Controller\ResultFactory;

class Edit extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        parent::__construct($context);
        $this->user = $user;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->authCheck() === false) {
            return $this->resultRedirectFactory->create()->setUrl($this->user->getUserSettingsUrl());
        }
        /** @var \Magento\Framework\View\Result\Layout $resultLayout */
        $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultLayout->getConfig()->getTitle()->prepend(__('Calendar Settings'));
        return $resultLayout;
    }

    private function authCheck()
    {
        $clientId = $this->user->getClientId();
        $clientSecret = $this->user->getClientSecret();
        $accessToken = $this->user->getAccessToken();

        return !empty($clientId) && !empty($clientSecret) && !empty($accessToken);
    }
}
