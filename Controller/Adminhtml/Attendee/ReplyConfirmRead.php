<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Attendee;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class ReplyConfirmRead extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository
     */
    private $eventAttendeeRepository;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository
    ) {
        parent::__construct($context);
        $this->eventAttendeeRepository = $eventAttendeeRepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws \InvalidArgumentException
     */
    public function execute()
    {
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        if ($this->_formKeyValidator->validate($this->getRequest())) {
            try {
                $request = $this->getRequest();
                $hash = $request->getParam('hashes');
                if ($hash) {
                    $this->eventAttendeeRepository->updateRows(
                        ['addressee_status' => 1],
                        ['event_attendee_hash IN (?)' => explode(',', $hash)]
                    );
                }
                $responseData = ['success' => true];
            } catch (\Exception $e) {
                $responseData = ['success' => false, 'errorMessage' => $e->getMessage()];
            }
        } else {
            $responseData = ['success' => false, 'errorMessage' => __('Form key is invalid')];
        }

        return $response->setData($responseData);
    }
}
