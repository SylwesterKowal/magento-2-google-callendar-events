<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Attendee;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class Action extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository
     */
    private $eventAttendeeRepository;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar,
        \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository
    ) {
        parent::__construct($context);
        $this->googleCalendar = $googleCalendar;
        $this->eventAttendeeRepository = $eventAttendeeRepository;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        if ($this->_formKeyValidator->validate($this->getRequest())) {
            try {
                $request = $this->getRequest();
                $hash = $request->getParam('event_attendee_hash');
                $responseStatus = $request->getParam('response_status');
                if (!empty($hash) &&
                    !empty($responseStatus) &&
                    in_array($responseStatus, ['accepted', 'tentative', 'declined'], false)) {
                    $additionalGuests = $request->getParam('additional_guests');
                    $comment = $request->getParam('comment');
                    if ($attendeeData = $this->eventAttendeeRepository->getRowById($hash)) {
                        /** @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $attendeeModel */
                        $attendeeModel = $this->eventAttendeeRepository->getPopulated($attendeeData);
                        $event = $this->googleCalendar->getEventById(
                            $attendeeModel->getCalendarGoogleId(),
                            $attendeeModel->getEventId()
                        );

                        $bind = ['response_status' => $responseStatus, 'addressee_status' => 0];
                        $attendees = $event->getAttendees();
                        /** @var \Google_Service_Calendar_EventAttendee[] $attendees */
                        foreach ($attendees as &$attendee) {
                            if ($attendee->getEmail() === $attendeeModel->getAttendeeEmail()) {
                                $attendee->setResponseStatus($responseStatus);
                                if ($additionalGuests) {
                                    $attendee->setAdditionalGuests($additionalGuests);
                                    $bind['additional_guests'] = $additionalGuests;
                                }
                                if ($comment) {
                                    $attendee->setComment($comment);
                                    $bind['comment'] = $comment;
                                }
                                if (!$attendee->getDisplayName() && $attendeeModel->getAttendeeDisplayName()) {
                                    $attendee->setDisplayName($attendeeModel->getAttendeeDisplayName());
                                }
                                break;
                            }
                        }
                        unset($attendee);
                        $event->setAttendees($attendees);
                        $this->googleCalendar->getUpdateEvent(
                            $attendeeModel->getCalendarGoogleId(),
                            $attendeeModel->getEventId(),
                            $event
                        );
                        $this->eventAttendeeRepository->updateRows($bind, ['event_attendee_hash = ?' => $hash]);
                    }
                }
                $responseData = ['success' => true];
            } catch (\Exception $e) {
                $responseData = ['success' => false, 'errorMessage' => $e->getMessage()];
            }
        } else {
            $responseData = ['success' => false, 'errorMessage' => __('Form key is invalid')];
        }

        return $response->setData($responseData);
    }
}
