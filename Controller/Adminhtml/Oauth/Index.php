<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Oauth;

class Index extends \Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $calendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        parent::__construct($context);
        $this->calendar = $calendar;
        $this->user = $user;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @inheritdoc
     */
    public function execute()
    {
        $authCode = $this->getRequest()->getParam('code');
        if (empty($authCode)) {
            $this->messageManager->addErrorMessage(__('No authorization code was passed.'));
        } else {
            $accessToken = $this->calendar->getAccessToken($authCode);
            if (!empty($accessToken)) {
                $this->user->saveAccessToken(json_encode($accessToken));
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath(
            'adminhtml/user/edit/user_id/' . $this->user->getId(),
            ['active_tab' => 'celesta_settings']
        );
    }
}
