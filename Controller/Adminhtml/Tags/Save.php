<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Tags;

use Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage;

class Save extends Manage
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\TagRepository
     */
    private $tagRepository;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository
    ) {
        parent::__construct($context);
        $this->tagRepository = $tagRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $general = $this->getRequest()->getParam('general', []);
        $tagsTitles = [];
        if (!empty($general['tags'])) {
            $tagsTitles = explode(',', $general['tags']);
            $tagsTitles = array_map('trim', $tagsTitles);
            $tagsTitles = array_unique(array_filter($tagsTitles));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $this->tagRepository->saveAllTitles($tagsTitles);
            $this->messageManager->addSuccessMessage(__('Tags saved.'));

            return $resultRedirect->setPath(\Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . '*/');
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $resultRedirect;
    }
}
