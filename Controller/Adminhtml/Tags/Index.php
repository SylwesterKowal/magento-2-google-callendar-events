<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Tags;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Celesta\AdvancedGoogleCalendar\Controller\Adminhtml\Manage
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \InvalidArgumentException
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Layout $resultLayout */
        $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultLayout->getConfig()->getTitle()->prepend(__('Calendar Event Tags'));
        return $resultLayout;
    }
}
