<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Magento\Framework\App\Cache\Type\Config;

class User
{
    /**
     * @var \Magento\Framework\HTTP\Adapter\CurlFactory
     */
    private $curlFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $cacheTypeList;
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    private $authSession;

    /**
     * @var \Magento\User\Model\UserFactory
     */
    private $userFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var CalendarRepository
     */
    private $calendarRepository;

    /**
     * @var string[]
     */
    private $userCalendars;

    /**
     * @var int
     */
    private $userId;

    /**
     * @param \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\User\Model\UserFactory $userFactory
     * @param CalendarRepository $calendarRepository
     * @param null $userId
     */
    public function __construct(
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\User\Model\UserFactory $userFactory,
        CalendarRepository $calendarRepository,
        $userId = null
    )
    {
        $this->curlFactory = $curlFactory;
        $this->coreRegistry = $coreRegistry;
        $this->url = $url;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
        $this->authSession = $authSession;
        $this->userFactory = $userFactory;
        $this->calendarRepository = $calendarRepository;
        $this->userId = $userId ? (int)$userId : null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        if ($this->userId === null) {
            $this->userId = (int)$this->authSession->getUser()->getId();
        }
        return $this->userId;
    }

    public function setUserId($user_id)
    {
        $this->userId = (int)$user_id;
    }

    public function isSessionUser()
    {
        return (int)$this->userId === (int)$this->authSession->getUser()->getId();
    }

    public function dropAuth()
    {
        $this->configWriter->delete($this->getConfigClientIdPath());
        $this->configWriter->delete($this->getConfigClientSecretPath());

        $configUsersKey = Module::CONFIG_PATH . '/user_ids';
        $calendarUsers = $this->scopeConfig->getValue($configUsersKey);
        $calendarUsers = $calendarUsers ? explode(',', $calendarUsers) : [];
        $key = array_search($this->getId(), $calendarUsers, false);
        if ($key !== false) {
            unset($calendarUsers[$key]);
            if ($calendarUsers) {
                $this->configWriter->save($key, implode(',', $calendarUsers));
            } else {
                $this->configWriter->delete($configUsersKey);
            }
        }
    }

    public function dropAccessToken()
    {
        $this->configWriter->delete($this->getConfigAccessTokenPath());
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
    }

    /**
     * @return int[]
     */
    public function getUserCalendarGoogleIds()
    {
        $calendarIds = [];
        if ($calendars = $this->getCalendars()) {
            foreach ($calendars as $calendar) {
                $calendarIds[] = $calendar->getGoogleId();
            }
        }
        return $calendarIds;
    }

    /**
     * @param string $calendarGoogleId
     * @param bool $graceful
     * @return Calendar|null
     * @throws \Magento\Framework\Exception\ConfigurationMismatchException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getUserCalendarByGoogleId($calendarGoogleId, $graceful = false)
    {
        if ($calendars = $this->getCalendars()) {
            foreach ($calendars as $calendar) {
                if ($calendarGoogleId === $calendar->getGoogleId()) {
                    return $calendar;
                }
            }
        }
        if ($graceful === false) {
            throw new \Magento\Framework\Exception\ConfigurationMismatchException(
                __('No calendar with Google ID %1 assigned to user', $calendarGoogleId)
            );
        }
        return null;
    }

    /**
     * @param string $calendarId
     * @param bool $graceful
     * @return Calendar|null
     * @throws \Magento\Framework\Exception\ConfigurationMismatchException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getUserCalendarById($calendarId, $graceful = false)
    {
        if ($calendars = $this->getCalendars()) {
            foreach ($calendars as $calendar) {
                if ($calendarId == $calendar->getId()) {
                    return $calendar;
                }
            }
        }
        if ($graceful === false) {
            throw new \Magento\Framework\Exception\ConfigurationMismatchException(
                __('No calendar with ID %1 assigned to user', $calendarId)
            );
        }
        return null;
    }

    public function getUserCalendarByIdCustom($calendarId, $graceful = false)
    {
        if ($calendars = $this->getCalendars()) {
            foreach ($calendars as $calendar) {
                if ($calendarId == $calendar->getId()) {
                    return $calendar;
                }
            }
        }
        return null;
    }

    /**
     * @param bool $force
     * @return Calendar[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCalendars($force = false)
    {
        if ($this->userCalendars === null || $force) {
            $this->userCalendars = $this->calendarRepository->getCalendarsByUserId($this->getId());
        }
        return $this->userCalendars;
    }

    /**
     * @param bool $force
     * @return Calendar[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getConnectedCalendars($force = false)
    {
        return array_filter($this->getCalendars($force), function (Calendar $c) {
            return (bool)$c->getIsConnected();
        });
    }

    /**
     * @param bool $force
     * @return Calendar[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDashboardCalendars($force = false)
    {
        return array_filter($this->getCalendars($force), function (Calendar $c) {
            return (bool)$c->getIsDashboard();
        });
    }

    /**
     * @param string $accessToken
     * @return bool
     */
    public function isAccessTokenValid($accessToken)
    {
        try {
            if ($accessToken) {
                $curl = $this->curlFactory->create();
                $curl->setConfig(['header' => false]);
                $curl->write('GET', 'https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=' . $accessToken);
                $content = $curl->read();
                $content = json_decode($content, true);
                if (empty($content['error_description']) && !empty($content['scope'])) {
                    return true;
                }
            }
        } catch (\Exception $e) {
        }
        return false;
    }

    /**
     * @return string
     */
    public function getUserSettingsUrl()
    {
        return $this->url->getUrl('adminhtml/user/edit/user_id/' . $this->getId(), ['active_tab' => 'celesta_settings']);
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->scopeConfig->getValue($this->getConfigClientIdPath($this->getId()));
    }

    /**
     * @param string $clientId
     */
    public function saveClientId($clientId)
    {
        $this->configWriter->save($this->getConfigClientIdPath($this->getId()), $clientId);
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->scopeConfig->getValue($this->getConfigClientSecretPath($this->getId()));
    }

    /**
     * @param string $clientSecret
     */
    public function saveClientSecret($clientSecret)
    {
        $this->configWriter->save($this->getConfigClientSecretPath($this->getId()), $clientSecret);
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
    }

    /**
     * @param string $key
     * @return string|array
     * @throws \Celesta\AdvancedGoogleCalendar\Model\Exception
     */
    public function getAccessToken($key = null)
    {
        $accessToken = $this->scopeConfig->getValue($this->getConfigAccessTokenPath());
        $accessToken = json_decode($accessToken, true);
        if ($key === null) {
            return $accessToken;
        } elseif ($key === 'refresh_token' && empty($accessToken['refresh_token'])) {
            $refreshToken = $this->scopeConfig->getValue($this->getConfigRefreshTokenPath());
            if (empty($refreshToken)) {
                throw new \Celesta\AdvancedGoogleCalendar\Model\Exception(__('Google Refresh token is missing'), null, Exception::REASON_GOOGLE_REFRESH_TOKEN_IS_MISSING);
            }
            $accessToken['refresh_token'] = $refreshToken;
        }
        return $accessToken[$key];
    }

    /**
     * @param string $accessToken
     */
    public function saveAccessToken($accessToken)
    {
        $this->configWriter->save($this->getConfigAccessTokenPath($this->getId()), $accessToken);
        $accessToken = json_decode($accessToken, true);
        if ($accessToken && !empty($accessToken['refresh_token'])) {
            $this->configWriter->save($this->getConfigRefreshTokenPath($this->getId()), $accessToken['refresh_token']);
        }
        $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
    }

    private function getConfigClientIdPath()
    {
        return Module::VENDOR . '/user-' . $this->getId() . '/google_api/client_id';
    }

    private function getConfigClientSecretPath()
    {
        return Module::VENDOR . '/user-' . $this->getId() . '/google_api/client_secret';
    }

    private function getConfigAccessTokenPath()
    {
        return Module::VENDOR . '/user-' . $this->getId() . '/google_api/auth_token';
    }

    private function getConfigRefreshTokenPath()
    {
        return Module::VENDOR . '/user-' . $this->getId() . '/google_api/refresh_token';
    }
}
