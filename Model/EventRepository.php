<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @param ResourceModel\Event $resource
 */
class EventRepository extends AbstractRepository
{
    /**
     * @var EventFactory
     */
    private $eventFactory;

    /**
     * @param EventFactory $eventFactory
     * @param ResourceModel\Event $resource
     */
    public function __construct(
        EventFactory $eventFactory,
        ResourceModel\Event $resource
    ) {
        parent::__construct($resource, 'event');
        $this->eventFactory = $eventFactory;
    }

    /**
     * @param string $eventHash
     * @return Event
     * @throws NoSuchEntityException
     */
    public function getById($eventHash)
    {
        /** @var Event $event */
        $event = $this->eventFactory->create();
        $this->resource->load($event, $eventHash);
        if ($event->getId()) {
            return $event;
        }
        throw new NoSuchEntityException(__('Requested event does not exist'));
    }
}
