<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class EventAttendeePersistence
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventDateTime
     */
    private $eventDateTime;

    /**
     * @var EventAttendeeRepository
     */
    private $eventAttendeeRepository;

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime
     * @param EventAttendeeRepository $eventAttendeeRepository
     */
    public function __construct(
        \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime,
        EventAttendeeRepository $eventAttendeeRepository
    ) {
        $this->eventDateTime = $eventDateTime;
        $this->eventAttendeeRepository = $eventAttendeeRepository;
    }

    /**
     * @param \Google_Service_Calendar_Event[] $calendarEvents
     * @param string $calendarGoogleId
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function syncEventsAttendees(array $calendarEvents, $calendarGoogleId)
    {
        $rowsToInsert = [];
        $rowsEventHashes = [];
        foreach ($calendarEvents as $calendarEvent) {
            if (!$this->eventDateTime->getHasEventPassed($calendarEvent)) {
                $eventHash = md5($calendarGoogleId . $calendarEvent->getId());
                $rowsEventHashes[] = $eventHash;
                $attendees = $calendarEvent->getAttendees();
                if ($attendees) {
                    /** @var \Google_Service_Calendar_EventAttendee[] $attendees */
                    foreach ($attendees as $attendee) {
                        $attendeeEmail = $attendee->getEmail();
                        $eventAttendeeHash = md5($calendarGoogleId . $calendarEvent->getId() . $attendeeEmail);
                        $rowsToInsert[$eventAttendeeHash] = [
                            'event_attendee_hash' => $eventAttendeeHash,
                            'event_hash' => $eventHash,
                            'event_id' => $calendarEvent->getId(),
                            'calendar_google_id' => $calendarGoogleId,
                            'creator_email' => $calendarEvent->getCreator()->getEmail(),
                            'attendee_email' => $attendeeEmail,
                            'attendee_display_name' => $attendee->getDisplayName(),
                            'response_status' => $attendee->getResponseStatus(),
                            'additional_guests' => $attendee->getAdditionalGuests() ?: 0,
                            'comment' => $attendee->getComment(),
                            'addressee_status' => 0
                        ];
                    }
                }
            }
        }

        $dbAttendees = $this->eventAttendeeRepository->getAttendeesByEventHashes($rowsEventHashes);

        // Updating attendees living only addressee_status
        $eventActualRows = [];
        if ($rowsToInsert) {
            foreach ($rowsToInsert as $hash => $row) {
                $eventActualRows[] = $row['event_attendee_hash'];
                foreach ($dbAttendees as $dbAttendee) {
                    if ($row['event_attendee_hash'] === $dbAttendee['event_attendee_hash']) {
                        if ($row['response_status'] === $dbAttendee['response_status']) {
                            $row['addressee_status'] = (int)$dbAttendee['addressee_status'];
                        }
                        $this->eventAttendeeRepository->save(
                            $this->eventAttendeeRepository->getPopulated($row)
                        );
                        unset($rowsToInsert[$hash]);
                    }
                }
            }
            if ($rowsToInsert) {
                $this->eventAttendeeRepository->insertRows($rowsToInsert);
            }
        }

        // Deleting missing attendees
        if ($dbAttendees) {
            $eventDbRows = array_column($dbAttendees, 'event_attendee_hash');
            $dbAttendeesToDelete = array_diff($eventDbRows, $eventActualRows);
            if ($dbAttendeesToDelete) {
                $this->eventAttendeeRepository->deleteRows(['event_attendee_hash IN (?)' => $dbAttendeesToDelete]);
            }
        }
    }
}
