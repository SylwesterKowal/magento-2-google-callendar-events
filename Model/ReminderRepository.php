<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

/**
 * @param ResourceModel\Reminder $resource
 */
class ReminderRepository extends AbstractRepository
{
    /**
     * @var ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var ResourceModel\Calendar
     */
    private $calendarResource;

    /**
     * @param ResourceModel\Reminder $resource
     * @param ResourceModel\Calendar $calendarResource
     * @param ReminderFactory $reminderFactory
     */
    public function __construct(
        ResourceModel\Reminder $resource,
        ResourceModel\Calendar $calendarResource,
        ReminderFactory $reminderFactory
    ) {
        parent::__construct($resource, 'reminder');
        $this->calendarResource = $calendarResource;
        $this->reminderFactory = $reminderFactory;
    }

    /**
     * @return Reminder[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getActualReminders()
    {
        $select = $this->getConnection()->select();
        $select
            ->from(['main_table' => $this->resource->getMainTable()])
            ->joinLeft(['c' => $this->calendarResource->getMainTable()], 'main_table.calendar_id = c.calendar_id', [])
            ->where('start < NOW() AND end > NOW() AND is_connected = 1')
            ->group('event_id');

        $result = [];
        $queryResult = $this->getConnection()->fetchAll($select);
        if ($queryResult) {
            foreach ($queryResult as $data) {
                $result[] = $this->getPopulated($data);
            }
        }
        return $result;
    }

    /**
     * @param string[] $data
     * @return Reminder
     */
    private function getPopulated(array $data)
    {
        return $this->reminderFactory->create()
            ->setId($data[Reminder::KEY_REMINDER_HASH])
            ->setEventId($data[Reminder::KEY_EVENT_ID])
            ->setCalendarId($data[Reminder::KEY_CALENDAR_ID])
            ->setStart($data[Reminder::KEY_START])
            ->setEnd($data[Reminder::KEY_END]);
    }
}
