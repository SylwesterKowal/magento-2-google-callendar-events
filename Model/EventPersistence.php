<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class EventPersistence
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Api\Data\EventInterfaceFactory
     */
    private $eventFactory;

    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var EventPersistence
     */
    private $eventAttendeePersistence;

    /**
     * @var ReminderPersistence
     */
    private $reminderPersistence;

    /**
     * @param EventFactory $eventFactory
     * @param EventRepository $eventRepository
     * @param TagRepository $tagRepository
     * @param ReminderPersistence $reminderPersistence
     */
    public function __construct(
        \Celesta\AdvancedGoogleCalendar\Model\EventFactory $eventFactory,
        EventRepository $eventRepository,
        TagRepository $tagRepository,
        EventAttendeePersistence $eventAttendeePersistence,
        ReminderPersistence $reminderPersistence
    ) {
        $this->eventFactory = $eventFactory;
        $this->eventRepository = $eventRepository;
        $this->tagRepository = $tagRepository;
        $this->eventAttendeePersistence = $eventAttendeePersistence;
        $this->reminderPersistence = $reminderPersistence;
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param \Google_Service_Calendar_Event[][] $googleEventArrays
     * @param callable $tokenUpdate
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function syncCalendarEvents(\Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar, array $googleEventArrays, callable $tokenUpdate)
    {
        $eventHashes = [];
        $deleteEventIds = [];
        $newAndUpdated = [];
        $calendarGoogleId = $calendar->getGoogleId();
        foreach ($googleEventArrays as $calendarEvents) {
            /** @var \Google_Service_Calendar_Event[] $calendarEvents */
            foreach ($calendarEvents as $event) {
                if ($event->getStatus() === 'confirmed') {
                    $newAndUpdated[] = $event;
                } else {
                    $deleteEventIds[] = $event->getId();
                    $eventHashes[] = md5($calendarGoogleId . $event->getId());
                }
            }
        }
        $connection = $this->eventRepository->getConnection();
        $connection->beginTransaction();
        try {
            $this->eventRepository->deleteRows(['event_hash IN (?)' => $eventHashes]);
            $this->saveCalendarEvents($calendar, $newAndUpdated);
            $this->eventAttendeePersistence->syncEventsAttendees($newAndUpdated, $calendarGoogleId);
            $this->reminderPersistence->syncEventsReminders($newAndUpdated, $calendar, $deleteEventIds);
            $tokenUpdate();
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Could not save events'), $e);
        }
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param \Google_Service_Calendar_Event[] $calendarEvents
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    private function saveCalendarEvents(\Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar, array $calendarEvents)
    {
        $eventTags = [];
        $emptyEventHashes = [];
        $calendarId = $calendar->getId();
        $calendarGoogleId = $calendar->getGoogleId();
        foreach ($calendarEvents as $calendarEvent) {
            $eventId = $calendarEvent->getId();
            $eventHash = md5($calendarGoogleId . $eventId);

            $event = $this->eventFactory->create();
            $event->isObjectNew(true);
            $event->setId($eventHash);
            $event->setEventId($eventId);
            $event->setSummary($calendarEvent->getSummary());
            $event->setCalendarId($calendarId);
            $event->setColorId((int)$calendarEvent->getColorId());
            $event->setRecurrence((int)$calendarEvent->getRecurrence());
            $event->setTransparency((int)$calendarEvent->getTransparency());
            $event->setVisibility((int)$calendarEvent->getVisibility());

            $isAllDay = (bool)$calendarEvent->getStart()->getDate();
            $event->setAllDay((int)$isAllDay);
            if ($isAllDay) {
                $start = new \DateTimeImmutable($calendarEvent->getStart()->getDate(), new \DateTimeZone('UTC'));
                $end = new \DateTimeImmutable($calendarEvent->getEnd()->getDate(), new \DateTimeZone('UTC'));
            } else {
                $start = new \DateTime($calendarEvent->getStart()->getDateTime());
                $start->setTimezone(new \DateTimeZone('UTC'));
                $end = new \DateTime($calendarEvent->getEnd()->getDateTime());
                $end->setTimezone(new \DateTimeZone('UTC'));
            }
            $event->setStartUTC($start);
            $event->setEndUTC($end);

            $event->setHtmlLink($calendarEvent->getHtmlLink());
            $this->eventRepository->save($event);

            $emptyEventHashes[] = $eventHash;
            $extendedProperties = $calendarEvent->getExtendedProperties();
            if ($extendedProperties) {
                $shared = $extendedProperties->getShared();
                if (!empty($shared['tags'])) {
                    $tags = array_filter(explode(',', $shared['tags']));
                    if ($tags) {
                        $eventTags[$eventHash] = $tags;
                    }
                }
            }
        }
        $this->tagRepository->deleteRowsFromPivot(['event_hash IN (?)' => $emptyEventHashes]);
        if ($eventTags) {
            $this->saveTags($eventTags);
        }
    }

    /**
     * @param array[] $inputEventTags
     */
    private function saveTags(array $inputEventTags)
    {
        $inputTagTitles = [];
        foreach ($inputEventTags as $tagTitles) {
            foreach ($tagTitles as $tagTitle) {
                $inputTagTitles[] = $tagTitle;
            }
        }
        $inputTagTitles = array_unique($inputTagTitles);
        sort($inputTagTitles);

        $rowsToInsert = [];
        $allTags = $this->tagRepository->getAllTags();
        $existingTitles = array_column($allTags, 'tag_title');
        foreach ($inputTagTitles as $inputTagTitle) {
            if (!in_array($inputTagTitle, $existingTitles, false)) {
                $rowsToInsert[] = ['tag_title' => $inputTagTitle];
            }
        }
        if ($rowsToInsert) {
            $this->tagRepository->insertRows($rowsToInsert);
        }
        $pivotData = $this->tagRepository->getTagIdsByEventHashes(array_keys($inputEventTags));
        $eventExistingTags = [];
        foreach ($pivotData as $row) {
            $eventExistingTags[$row['event_hash']][] = 'tag_id';
        }
        $rowsToInsert = [];
        $allTags = array_column($this->tagRepository->getAllTags(), 'tag_title', 'tag_id');
        $getIdsByTitles = function (array $tagTitles) use ($allTags) {
            $ids = [];
            foreach ($tagTitles as $tagTitle) {
                $ids[] = array_search($tagTitle, $allTags, false);
            }

            return $ids;
        };
        foreach ($inputEventTags as $eventHash => $tagTitles) {
            $tagsIds = $getIdsByTitles($tagTitles);
            if (isset($eventExistingTags[$eventHash])) {
                $insertIds = array_diff($tagsIds, $eventExistingTags[$eventHash]);
                $deleteIds = array_diff($eventExistingTags[$eventHash], $tagsIds);
                if ($deleteIds) {
                    $this->tagRepository->deleteRowsFromPivot(
                        ['event_hash = ?' => $eventHash, 'tag_id IN (?)' => $deleteIds]
                    );
                }
            } else {
                $insertIds = $tagsIds;
            }
            foreach ($insertIds as $missingId) {
                $rowsToInsert[] = ['event_hash' => $eventHash, 'tag_id' => $missingId];
            }
        }
        if ($rowsToInsert) {
            $this->tagRepository->insertRowsToPivot($rowsToInsert);
        }
    }
}
