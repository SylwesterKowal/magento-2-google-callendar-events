<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

/**
 * Reminder resource model
 */
class Reminder extends \Magento\Framework\Model\AbstractModel implements \Celesta\AdvancedGoogleCalendar\Api\Data\ReminderInterface
{
    const KEY_REMINDER_HASH = 'event_id';
    const KEY_EVENT_ID = 'event_id';
    const KEY_CALENDAR_ID = 'calendar_id';
    const KEY_START = 'start';
    const KEY_END = 'end';

    protected $_idFieldName = self::KEY_REMINDER_HASH;

    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Reminder::class);
    }

    /**
     * @return string
     */
    public function getEventId()
    {
        return $this->getData(self::KEY_EVENT_ID);
    }

    /**
     * @param string $eventId
     * @return $this
     */
    public function setEventId($eventId)
    {
        return $this->setData(self::KEY_EVENT_ID, $eventId);
    }

    /**
     * @return int
     */
    public function getCalendarId()
    {
        return $this->getData(self::KEY_CALENDAR_ID);
    }

    /**
     * @param int $calendarId
     * @return $this
     */
    public function setCalendarId($calendarId)
    {
        return $this->setData(self::KEY_CALENDAR_ID, $calendarId);
    }

    /**
     * @return string|\DateTimeInterface
     */
    public function getStart()
    {
        return $this->getData(self::KEY_START);
    }

    /**
     * @param string|\DateTimeInterface $start
     * @return $this
     */
    public function setStart($start)
    {
        return $this->setData(self::KEY_START, $start);
    }

    /**
     * @return string|\DateTimeInterface
     */
    public function getEnd()
    {
        return $this->getData(self::KEY_END);
    }

    /**
     * @param string|\DateTimeInterface $end
     * @return $this
     */
    public function setEnd($end)
    {
        return $this->setData(self::KEY_END, $end);
    }
}
