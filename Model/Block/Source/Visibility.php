<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\Block\Source;

class Visibility implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper
     */
    private $valueMapper;

    public function __construct(\Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $valueMapper)
    {
        $this->valueMapper = $valueMapper;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $map = $this->valueMapper->getVisibilityMap();
        $options = [];
        foreach ($map as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key
            ];
        }

        return $options;
    }
}
