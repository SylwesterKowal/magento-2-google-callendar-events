<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\Block\Source;

class Tag implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\TagRepository
     */
    private $tagRepository;

    public function __construct(\Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $tags = $this->tagRepository->getAllTags();
        $options = [];
        foreach ($tags as $key => $tag) {
            $options[] = [
                'label' => $tag['tag_title'],
                'value' => $tag['tag_id']
            ];
        }

        return $options;
    }
}
