<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\Block\Source;

class Calendar implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    public function __construct(\Celesta\AdvancedGoogleCalendar\Model\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $calendars = $this->user->getConnectedCalendars();
        $options = [];
        foreach ($calendars as $key => $calendar) {
            $options[] = [
                'label' => $calendar->getSummary(),
                'value' => $calendar->getId()
            ];
        }

        return $options;
    }
}
