<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\Block;

use Magento\Framework\Exception\LocalizedException;

class EventValueMapper
{
    const FIELD_TRANSPARENCY_BUSY = 0;
    const FIELD_TRANSPARENCY_AVAILABLE = 1;

    const FIELD_VISIBILITY_DEFAULT = 0;
    const FIELD_VISIBILITY_PUBLIC = 1;
    const FIELD_VISIBILITY_PRIVATE = 2;

    const FIELD_RECURRENCE_SINGLE = 0;
    const FIELD_RECURRENCE_RECURRENT = 1;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    public function __construct(\Celesta\AdvancedGoogleCalendar\Model\User $user)
    {
        $this->user = $user;
    }

    public function getTransparencyLabel($key)
    {
        return $this->getTransparencyMap()[(int)$key];
    }

    public function getTransparencyMap()
    {
        return [
            self::FIELD_TRANSPARENCY_BUSY => __('busy'),
            self::FIELD_TRANSPARENCY_AVAILABLE => __('available')
        ];
    }

    public function getTransparencyGoogle($key)
    {
        return $this->getTransparencyGoogleMap()[(int)$key];
    }

    public function getTransparencyKeyByGoogleValue($value)
    {
        return array_search($value, $this->getTransparencyGoogleMap(), false);
    }

    public function getTransparencyGoogleMap()
    {
        return [
            self::FIELD_TRANSPARENCY_BUSY => 'opaque',
            self::FIELD_TRANSPARENCY_AVAILABLE => 'transparent'
        ];
    }

    public function getVisibilityLabel($key)
    {
        return $this->getVisibilityMap()[(int)$key];
    }

    public function getVisibilityKey($label)
    {
        return array_search($label, $this->getVisibilityMap(), null);
    }

    public function getVisibilityKeyByGoogleValue($value)
    {
        return array_search($value, $this->getVisibilityGoogleMap(), false);
    }

    public function getVisibilityGoogle($key)
    {
        return $this->getVisibilityGoogleMap()[(int)$key];
    }

    public function getVisibilityGoogleMap()
    {
        return [
            self::FIELD_VISIBILITY_DEFAULT => 'default',
            self::FIELD_VISIBILITY_PUBLIC => 'public',
            self::FIELD_VISIBILITY_PRIVATE => 'private'
        ];
    }

    public function getVisibilityMap()
    {
        return [
            self::FIELD_VISIBILITY_DEFAULT => __('default'),
            self::FIELD_VISIBILITY_PUBLIC => __('public'),
            self::FIELD_VISIBILITY_PRIVATE => __('private')
        ];
    }

    public function getRecurrenceLabel($key)
    {
        return $this->getRecurrenceMap()[(int)$key];
    }

    public function getRecurrenceMap()
    {
        return [
            self::FIELD_RECURRENCE_SINGLE => __('single'),
            self::FIELD_RECURRENCE_RECURRENT => __('recurrent')
        ];
    }
}
