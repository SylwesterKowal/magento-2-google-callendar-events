<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class ReminderPersistence
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    private $timezone;

    /**
     * @var ReminderFactory
     */
    private $reminderFactory;

    /**
     * @var ReminderRepository
     */
    private $repository;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\Timezone $timezone
     * @param ReminderRepository $repository
     * @param ReminderFactory $reminderFactory
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        ReminderRepository $repository,
        ReminderFactory $reminderFactory
    ) {
        $this->timezone = $timezone;
        $this->reminderFactory = $reminderFactory;
        $this->repository = $repository;
    }

    /**
     * @param \Google_Service_Calendar_Event[] $events
     * @param Calendar $calendar
     * @param array $deleteReminderHashes
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function syncEventsReminders(array $events, Calendar $calendar, array $deleteReminderHashes = [])
    {
        $prepared = [];
        foreach ($events as $event) {
            $extendedProperties = $event->getExtendedProperties();
            $hash = md5($event->getId() . $calendar->getId());
            if ($extendedProperties) {
                $shared = $extendedProperties->getShared();
                if (!empty($shared[ReminderHelper::GOOGLE_CALENDAR_SYSTEM_KEY])) {
                    $eventStart = $event->getStart();
                    if ($eventStart->getDate()) {
                        $reminderEnd = new \DateTime($eventStart->getDate(), new \DateTimeZone($calendar->getTimeZone()));
                    } elseif ($eventStart->getDateTime()) {
                        $reminderEnd = new \DateTime($eventStart->getDateTime());
                    } else {
                        throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save system reminder'));
                    }
                    $now = new \DateTimeImmutable();
                    if ($reminderEnd > $now) { // event start is in the future
                        $reminderStart = clone $reminderEnd;
                        $reminderStart->modify('-' . $shared[ReminderHelper::GOOGLE_CALENDAR_SYSTEM_KEY] . ' minutes');
                        /** @var Reminder $r */
                        $r = $this->reminderFactory->create();
                        $r->setId($hash);
                        $r->setEventId($event->getId());
                        $r->setCalendarId($calendar->getId());
                        $r->setStart($reminderStart);
                        $r->setEnd($reminderEnd);
                        $prepared[$event->getId()] = $r;
                        continue;
                    }
                }
            }
            $deleteReminderHashes[] = $hash;
        }
        if ($deleteReminderHashes) {
            $this->repository->deleteRows(['reminder_hash IN (?)' => $deleteReminderHashes]);
        }
        foreach ($prepared as $r) {
            $this->repository->save($r);
        }
    }
}
