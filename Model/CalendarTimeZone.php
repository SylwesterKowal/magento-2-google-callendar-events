<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class CalendarTimeZone
{
    /**
     * @var array
     */
    private $calendar;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    private $timezone;

    /**
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
    ) {
        $this->timezone = $timezone;
        $this->calendar = $calendar;
    }

    public function getTimezone()
    {
        return $this->calendar->getUseSystemTimeZone() ? $this->timezone->getConfigTimezone() : $this->calendar->getTimeZone();
    }
}
