<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Celesta\AdvancedGoogleCalendar\Api\Data\EventInterface;

/**
 * Event resource model
 */
class Event extends \Magento\Framework\Model\AbstractModel implements \Celesta\AdvancedGoogleCalendar\Api\Data\EventInterface
{
    const KEY_HASH = 'event_hash';
    const KEY_EVENT_ID = 'event_id';
    const KEY_CALENDAR_ID = 'calendar_id';
    const KEY_SUMMARY = 'summary';
    const KEY_DESCRIPTION = 'description';
    const KEY_LOCATION = 'location';
    const KEY_COLOR_ID = 'color_id';
    const KEY_RECURRENCE = 'recurrence';
    const KEY_TRANSPARENCY = 'transparency';
    const KEY_VISIBILITY = 'visibility';
    const KEY_ALL_DAY = 'all_day';
    const KEY_START_UTC = 'start_utc';
    const KEY_END_UTC = 'end_utc';
    const KEY_HTML_LINK = 'html_link';

    protected $_idFieldName = self::KEY_HASH;

    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event::class);
    }

    /**
     * @return string
     */
    public function getEventId()
    {
        return $this->getData(self::KEY_EVENT_ID);
    }

    /**
     * @param string $eventId
     * @return $this
     */
    public function setEventId($eventId)
    {
        return $this->setData(self::KEY_EVENT_ID, $eventId);
    }

    /**
     * @return int
     */
    public function getCalendarId()
    {
        return $this->getData(self::KEY_CALENDAR_ID);
    }

    /**
     * @param int $calendarId
     * @return $this
     */
    public function setCalendarId($calendarId)
    {
        return $this->setData(self::KEY_CALENDAR_ID, $calendarId);
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->getData(self::KEY_SUMMARY);
    }

    /**
     * @param string $summary
     * @return $this
     */
    public function setSummary($summary)
    {
        return $this->setData(self::KEY_SUMMARY, $summary);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::KEY_DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(self::KEY_DESCRIPTION, $description);
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->getData(self::KEY_LOCATION);
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        return $this->setData(self::KEY_LOCATION, $location);
    }

    /**
     * @return int
     */
    public function getColorId()
    {
        return $this->getData(self::KEY_COLOR_ID);
    }

    /**
     * @param int $colorId
     * @return $this
     */
    public function setColorId($colorId)
    {
        return $this->setData(self::KEY_COLOR_ID, $colorId);
    }

    /**
     * @return int
     */
    public function getRecurrence()
    {
        return $this->getData(self::KEY_RECURRENCE);
    }

    /**
     * @param int $recurrence
     * @return $this
     */
    public function setRecurrence($recurrence)
    {
        return $this->setData(self::KEY_RECURRENCE, $recurrence);
    }

    /**
     * @return int
     */
    public function getTransparency()
    {
        return $this->getData(self::KEY_TRANSPARENCY);
    }

    /**
     * @param int $transparency
     * @return $this
     */
    public function setTransparency($transparency)
    {
        return $this->setData(self::KEY_TRANSPARENCY, $transparency);
    }

    /**
     * @return int
     */
    public function getVisibility()
    {
        return $this->getData(self::KEY_VISIBILITY);
    }

    /**
     * @param int $visibility
     * @return $this
     */
    public function setVisibility($visibility)
    {
        return $this->setData(self::KEY_VISIBILITY, $visibility);
    }

    /**
     * @return int
     */
    public function getAllDay()
    {
        return $this->getData(self::KEY_ALL_DAY);
    }

    /**
     * @param int $allDay
     * @return $this
     */
    public function setAllDay($allDay)
    {
        return $this->setData(self::KEY_ALL_DAY, $allDay);
    }

    /**
     * @return string|\DateTimeInterface
     */
    public function getStartUTC()
    {
        return $this->getData(self::KEY_START_UTC);
    }

    /**
     * @param string|\DateTimeInterface $start
     * @return $this
     */
    public function setStartUTC($start)
    {
        return $this->setData(self::KEY_START_UTC, $start);
    }

    /**
     * @return string|\DateTimeInterface
     */
    public function getEndUTC()
    {
        return $this->getData(self::KEY_END_UTC);
    }

    /**
     * @param string|\DateTimeInterface $end
     * @return $this
     */
    public function setEndUTC($end)
    {
        return $this->setData(self::KEY_END_UTC, $end);
    }

    /**
     * @return string
     */
    public function getHtmlLink()
    {
        return $this->getData(self::KEY_HTML_LINK);
    }

    /**
     * @param string $htmlLink
     * @return $this
     */
    public function setHtmlLink($htmlLink)
    {
        return $this->setData(self::KEY_HTML_LINK, $htmlLink);
    }
}
