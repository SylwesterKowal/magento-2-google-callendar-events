<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\ResourceModel;

class Reminder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init(\Celesta\AdvancedGoogleCalendar\Setup\InstallSchema::CALENDAR_REMINDER_TABLE_NAME, 'reminder_hash');
    }
}
