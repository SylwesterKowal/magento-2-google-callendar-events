<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event;

use Celesta\AdvancedGoogleCalendar\Setup\InstallSchema;

class Collection extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    /**
     * @var string
     */
    protected $_idFieldName = 'event_hash';

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag
     */
    private $tagResource;


    private $tagInitialized;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $tagResource
     * @param string $mainTable
     * @param string $resourceModel
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $tagResource,
        $mainTable = InstallSchema::CALENDAR_EVENT_TABLE_NAME,
        $resourceModel = \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event::class
    ) {
        $this->user = $user;
        $this->tagResource = $tagResource;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'tag_titles' && !empty($condition['in'])) {
            if (!$this->tagInitialized) {
                $this->tagInitialized = true;
                $tagIds = array_map('intval', $condition['in']);
                $subSelect = clone $this->getSelect();
                $subSelect->reset();
                $subSelect
                    ->from(
                        $this->getTable(InstallSchema::CALENDAR_TAG_PIVOT_TABLE_NAME),
                        [new \Zend_Db_Expr('DISTINCT event_hash')]
                    )
                    ->where('tag_id IN (?)', $tagIds);
                $this->_select->where('main_table.event_hash IN (?)', $subSelect);
            }
        } else {
            if ($field === 'calendar_summary') {
                $field = 'main_table.calendar_id';
            } elseif ($field === 'summary') {
                $field = 'main_table.summary';
            }
            parent::addFieldToFilter($field, $condition);
        }

        return $this;
    }

    protected function _beforeLoad()
    {
        parent::_beforeLoad();
//        $this->addFieldToSelect('calendar_id', 'calendar_summary');
        $tagSelect = $this->tagResource->getConnection()->select();
        $tagSelect
            ->from(
                ['t' => $this->tagResource->getMainTable()],
                ['GROUP_CONCAT(t.tag_title SEPARATOR ", ") AS tag_titles']
            )->joinRight(
                ['p' => $this->tagResource->getTable(InstallSchema::CALENDAR_TAG_PIVOT_TABLE_NAME)],
                'p.tag_id = t.tag_id',
                ['p.event_hash']
            )->group('p.event_hash');

        $this->getSelect()
            ->joinInner(
                ['c' => $this->tagResource->getTable(InstallSchema::CALENDAR_CALENDAR_TABLE_NAME)],
                'main_table.calendar_id = c.calendar_id',
                ['summary AS calendar_summary', 'calendar_google_id']
            )->joinLeft(
                ['p' => $tagSelect],
                'main_table.event_hash = p.event_hash',
                ['tag_titles']
            )->where('c.is_connected = 1 AND c.user_id = ?', $this->user->getId())
            ->distinct();

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(\Magento\Framework\DB\Select::ORDER);
        $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
        $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);
        $countSelect->reset(\Magento\Framework\DB\Select::COLUMNS);
        $countSelect->joinInner(
            ['c' => $this->tagResource->getTable(InstallSchema::CALENDAR_CALENDAR_TABLE_NAME)],
            'main_table.calendar_id = c.calendar_id',
            []
        );
        $countSelect->where('c.is_connected = 1 AND c.user_id = ?', $this->user->getId());
        $countSelect->columns('COUNT(*)');

        return $countSelect;
    }
}
