<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Celesta\AdvancedGoogleCalendar\Api\Data\CalendarInterface;

/**
 * Event resource model
 */
class Calendar extends \Magento\Framework\Model\AbstractModel implements \Celesta\AdvancedGoogleCalendar\Api\Data\CalendarInterface
{
    const KEY_ID = 'calendar_id';
    const KEY_GOOGLE_ID = 'calendar_google_id';
    const KEY_USER_ID = 'user_id';
    const KEY_SUMMARY = 'summary';
    const KEY_TIME_ZONE = 'time_zone';
    const KEY_USE_SYSTEM_TIME_ZONE = 'use_system_time_zone';
    const KEY_IS_PRIMARY = 'is_primary';
    const KEY_IS_CONNECTED = 'is_connected';
    const KEY_IS_DASHBOARD = 'is_dashboard';
    const KEY_SHARE_REMINDERS = 'share_reminders';
    const KEY_DESCRIPTION = 'description';
    const KEY_ETAG = 'etag';

    protected $_idFieldName = self::KEY_ID;

    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Calendar::class);
    }

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->getData(self::KEY_GOOGLE_ID);
    }

    /**
     * @param string $googleId
     * @return $this
     */
    public function setGoogleId($googleId)
    {
        return $this->setData(self::KEY_GOOGLE_ID, $googleId);
    }

    /**
     * @return INT
     */
    public function getUserId()
    {
        return $this->getData(self::KEY_USER_ID);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->getData(self::KEY_SUMMARY);
    }

    /**
     * @param string $summary
     * @return $this
     */
    public function setSummary($summary)
    {
        return $this->setData(self::KEY_SUMMARY, $summary);
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->getData(self::KEY_TIME_ZONE);
    }

    /**
     * @param string $timeZone
     * @return $this
     */
    public function setTimeZone($timeZone)
    {
        return $this->setData(self::KEY_TIME_ZONE, $timeZone);
    }

    /**
     * @return int
     */
    public function getUseSystemTimeZone()
    {
        return $this->getData(self::KEY_USE_SYSTEM_TIME_ZONE);
    }

    /**
     * @param int $useSystemTimeZone
     * @return $this
     */
    public function setUseSystemTimeZone($useSystemTimeZone)
    {
        return $this->setData(self::KEY_USE_SYSTEM_TIME_ZONE, $useSystemTimeZone);
    }

    /**
     * @return int
     */
    public function getIsPrimary()
    {
        return $this->getData(self::KEY_IS_PRIMARY);
    }

    /**
     * @param int $isPrimary
     * @return $this
     */
    public function setIsPrimary($isPrimary)
    {
        return $this->setData(self::KEY_IS_PRIMARY, $isPrimary);
    }

    /**
     * @return int
     */
    public function getIsConnected()
    {
        return $this->getData(self::KEY_IS_CONNECTED);
    }

    /**
     * @param int $isConnected
     * @return $this
     */
    public function setIsConnected($isConnected)
    {
        return $this->setData(self::KEY_IS_CONNECTED, $isConnected);
    }

    /**
     * @return int
     */
    public function getIsDashboard()
    {
        return $this->getData(self::KEY_IS_DASHBOARD);
    }

    /**
     * @param int $isDashboard
     * @return $this
     */
    public function setIsDashboard($isDashboard)
    {
        return $this->setData(self::KEY_IS_DASHBOARD, $isDashboard);
    }

    /**
     * @return int
     */
    public function getShareReminders()
    {
        return $this->getData(self::KEY_SHARE_REMINDERS);
    }

    /**
     * @param int $shareReminders
     * @return $this
     */
    public function setShareReminders($shareReminders)
    {
        return $this->setData(self::KEY_SHARE_REMINDERS, $shareReminders);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::KEY_DESCRIPTION);
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(self::KEY_DESCRIPTION, $description);
    }

    /**
     * @return string
     */
    public function getEtag()
    {
        return $this->getData(self::KEY_ETAG);
    }

    /**
     * @param string $etag
     * @return $this
     */
    public function setEtag($etag)
    {
        return $this->setData(self::KEY_ETAG, $etag);
    }
}
