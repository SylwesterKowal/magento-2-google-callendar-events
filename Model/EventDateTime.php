<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class EventDateTime
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\Timezone
     */
    private $timezone;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface
     */
    private $dateTimeFormatter;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Timezone $timezone,
        \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface $dateTimeFormatter
    ) {
        $this->timezone = $timezone;
        $this->dateTimeFormatter = $dateTimeFormatter;
    }

    /**
     * @param \Google_Service_Calendar_Event $event
     * @return bool
     */
    public function getHasEventPassed(\Google_Service_Calendar_Event $event)
    {
        $eventStart = $event->getStart();
        $start = new \DateTimeImmutable(
            $eventStart->getDate() ?: $eventStart->getDateTime(),
            new \DateTimeZone($this->timezone->getConfigTimezone())
        );
        $now = $this->timezone->date();

        return $now > $start;
    }

    /**
     * @param \Google_Service_Calendar_Event $event
     * @param \DateTimeZone $timeZone timezone display setting
     * @return string
     */
    public function getEventDateAndTimeReadable(\Google_Service_Calendar_Event $event, \DateTimeZone $timeZone)
    {
        if ($recurrence = $event->getRecurrence()) {
            $result = $this->getRecurrenceReadable($event);
        } elseif ($event->getStart()->getDateTime()) {
            $start = new \DateTime($event->getStart()->getDateTime());
            $start->setTimezone($timeZone);
            $result = $this->dateTimeFormatter->formatObject($start, \IntlDateFormatter::GREGORIAN);
            if ($event->getEnd()->getDateTime()) {
                $end = new \DateTime($event->getEnd()->getDateTime());
                $end->setTimezone($timeZone);
                $result .= ' - ' . $this->dateTimeFormatter->formatObject($end, \IntlDateFormatter::GREGORIAN);
            }
        } else {
            /** @see http://www.icu-project.org/apiref/icu4c/classSimpleDateFormat.html#details */
            $result = $this->dateTimeFormatter->formatObject(
                new \DateTime($event->getStart()->getDate()),
                'EEEE, d MMMM y'
            );
            if ($event->getEnd()->getDate()) {
                $result .= ' - ' . $this->dateTimeFormatter
                        ->formatObject(new \DateTime($event->getEnd()->getDate()), 'EEEE, d MMMM y');
            }
        }

        return $result;
    }

    /**
     * @param \Google_Service_Calendar_Event $event
     * @return string
     */
    public function getRecurrenceReadable(\Google_Service_Calendar_Event $event)
    {
        $readable = null;
        if ($recurrence = $event->getRecurrence()) {
            $recurrence = reset($recurrence); // RFC 5545
            $recurrence = new \RRule\RRule($recurrence);
            $formatter = $this->dateTimeFormatter;
            $readable = $recurrence->humanReadable([
                'date_formatter' => function (\DateTimeInterface $date) use ($formatter) {
                    /** @see http://www.icu-project.org/apiref/icu4c/classSimpleDateFormat.html#details */
                    return $formatter->formatObject($date, 'EEEE, d MMMM y');
                }
            ]);
            $readable = mb_strtoupper(mb_substr($readable, 0, 1)) . mb_substr($readable, 1);
        }

        return $readable;
    }
}
