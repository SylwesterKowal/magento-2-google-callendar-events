<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

/**
 * @api
 */
final class Module extends \Magento\Framework\DataObject
{
    const VENDOR = 'celesta';

    const NAME = 'advanced_google_calendar';

    const CONFIG_PATH = 'celesta/advanced_google_calendar';

    const CONFIG_PATH_GOOGLE_API_KEY = 'celesta_google_api/general/api_key';

    const CONFIG_PATH_CRON_SYNC_MIN = 'advanced_google_calendar/advanced_google_calendar_sync/cron_sync_minutes';

    const ADMIN_AREA_PATH = 'celesta/';
}
