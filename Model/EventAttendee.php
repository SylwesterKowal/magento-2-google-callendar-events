<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Celesta\AdvancedGoogleCalendar\Api\Data\EventAttendeeInterface;

/**
 * EventAttendee resource model
 */
class EventAttendee extends \Magento\Framework\Model\AbstractModel implements EventAttendeeInterface
{
    const KEY_EVENT_ATTENDEE_HASH = 'event_attendee_hash';
    const KEY_HASH = 'event_hash';
    const KEY_EVENT_ID = 'event_id';
    const KEY_CALENDAR_GOOGLE_ID = 'calendar_google_id';
    const KEY_CREATOR_EMAIL = 'creator_email';
    const KEY_ATTENDEE_EMAIL = 'attendee_email';
    const KEY_ATTENDEE_DISPLAY_NAME = 'attendee_display_name';
    const KEY_RESPONSE_STATUS = 'response_status';
    const KEY_ADDITIONAL_GUESTS = 'additional_guests';
    const KEY_COMMENT = 'comment';
    const KEY_ADDRESSEE_STATUS = 'addressee_status';

    protected $_idFieldName = self::KEY_EVENT_ATTENDEE_HASH;

    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Celesta\AdvancedGoogleCalendar\Model\ResourceModel\EventAttendee::class);
    }

    public function getEventHash()
    {
        return $this->getData(self::KEY_HASH);
    }

    public function setEventHash($eventHash)
    {
        return $this->setData(self::KEY_HASH, $eventHash);
    }

    public function getEventId()
    {
        return $this->getData(self::KEY_EVENT_ID);
    }

    public function setEventId($eventId)
    {
        return $this->setData(self::KEY_EVENT_ID, $eventId);
    }

    public function getCalendarGoogleId()
    {
        return $this->getData(self::KEY_CALENDAR_GOOGLE_ID);
    }

    public function setCalendarGoogleId($calendarId)
    {
        return $this->setData(self::KEY_CALENDAR_GOOGLE_ID, $calendarId);
    }

    public function getCreatorEmail()
    {
        return $this->getData(self::KEY_CREATOR_EMAIL);
    }

    public function setCreatorEmail($email)
    {
        return $this->setData(self::KEY_CREATOR_EMAIL, $email);
    }

    public function getAttendeeEmail()
    {
        return $this->getData(self::KEY_ATTENDEE_EMAIL);
    }

    public function setAttendeeEmail($email)
    {
        return $this->setData(self::KEY_ATTENDEE_EMAIL, $email);
    }

    public function getAttendeeDisplayName()
    {
        return $this->getData(self::KEY_ATTENDEE_DISPLAY_NAME);
    }

    public function setAttendeeDisplayName($displayName)
    {
        return $this->setData(self::KEY_ATTENDEE_DISPLAY_NAME, $displayName);
    }

    public function getResponseStatus()
    {
        return $this->getData(self::KEY_RESPONSE_STATUS);
    }

    public function setResponseStatus($responseStatus)
    {
        return $this->setData(self::KEY_RESPONSE_STATUS, $responseStatus);
    }

    public function getAdditionalGuests()
    {
        return $this->getData(self::KEY_ADDITIONAL_GUESTS);
    }

    public function setAdditionalGuests($additionalGuests)
    {
        return $this->setData(self::KEY_ADDITIONAL_GUESTS, $additionalGuests);
    }

    public function getComment()
    {
        return $this->getData(self::KEY_COMMENT);
    }

    public function setComment($comment)
    {
        return $this->setData(self::KEY_COMMENT, $comment);
    }

    public function getAddresseeStatus()
    {
        return $this->getData(self::KEY_ADDRESSEE_STATUS);
    }

    public function setAddresseeStatus($status)
    {
        return $this->setData(self::KEY_ADDRESSEE_STATUS, $status);
    }
}
