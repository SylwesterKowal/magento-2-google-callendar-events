<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Magento\Framework\Exception\LocalizedException;

class ExceptionHandler
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    public function __construct(\Magento\Framework\Message\ManagerInterface $messageManager)
    {
        $this->messageManager = $messageManager;
    }

    public function handle(\Exception $e)
    {
        if ($e instanceof \Google_Exception) {
            $this->handleGoogleException($e);
        } elseif ($e instanceof LocalizedException) {
            $this->handleLocalizedException($e);
        } else {
            $this->handleException($e);
        }
    }

    private function handleGoogleException(\Google_Exception $e)
    {
        $parsed = json_decode($e->getMessage(), true);
        if ($parsed && !empty($parsed['error'])) {
            $error = $parsed['error'];
            if (!empty($error['errors']) && is_array($error['errors'])) {
                foreach ($error['errors'] as $value) {
                    if (!empty($value['message'])) {
                        $this->messageManager->addErrorMessage('Request to Google failed. ' . $value['message']);
                    }
                }
            } elseif (!empty($parsed['message'])) {
                $this->messageManager->addErrorMessage('Request to Google failed. ' . $parsed['message']);
            }
        }
    }

    private function handleLocalizedException(LocalizedException $e)
    {
        $this->messageManager->addErrorMessage($e->getMessage());
    }

    private function handleException(\Exception $e)
    {
        $this->messageManager->addErrorMessage($e->getMessage());
    }
}
