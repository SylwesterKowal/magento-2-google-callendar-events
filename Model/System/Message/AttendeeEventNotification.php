<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\System\Message;

class AttendeeEventNotification extends Base
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ReminderRepository
     */
    private $eventAttendeeRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee[]
     */
    private $attendees;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    private $layout;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventDateTime
     */
    private $eventDateTime;

    /**
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime
     */
    public function __construct(
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\View\LayoutInterface $layout,
        \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository $eventAttendeeRepository,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar,
        \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime
    ) {
        parent::__construct($authorization);
        $this->layout = $layout;
        $this->eventAttendeeRepository = $eventAttendeeRepository;
        $this->user = $user;
        $this->googleCalendar = $calendar;
        $this->eventDateTime = $eventDateTime;
    }

    public function getIdentity()
    {
        $identity = null;
        if ($this->isAllowed()) {
            $ids = [];
            foreach ($this->getAttendees() as $attendee) {
                $ids[] = $attendee->getId();
            }
            $identity = \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH . '/AttendeeEventNotification/' .
                md5(implode('', $ids));
        }

        return $identity;
    }

    /**
     * @inheritdoc
     */
    public function isDisplayed()
    {
        return $this->isAllowed() && (bool)$this->getAttendees();
    }

    /**
     * @inheritdoc
     */
    public function getText()
    {
        $content = '';
        $invitations = [];
        $replies = [];
        foreach ($this->getAttendees() as $attendee) {
            if ($attendee->getResponseStatus() === 'needsAction') {
                $invitations[] = $this->getActionContent($attendee);
            } else {
                $replies[$attendee->getEventId()][] = $attendee;
            }
        }
        if ($invitations) {
            $content .=
                '<div class="celesta_event_attendance_header">' .
                __('You are invited to the event(s). Are you going to participate?') .
                '</div>' . implode('<hr class="celesta_event_hr">', $invitations);
        }
        if ($replies) {
            $repliesContents = [];
            foreach ($replies as $eventId => $attendees) {
                $repliesContents[] = $this->getReplyContent($attendees);
            }
            $separator = $invitations ? ' style="border-top:1px dashed #d1d1d1;margin-top:5px;padding-top:5px;"' : '';
            $content .=
                '<div class="celesta_event_attendance_header"' . $separator . '>' .
                __('There are some replies on the event you organized:') .
                '</div>' . implode('<hr class="celesta_event_hr">', $repliesContents);
        }

        return $content;
    }

    /**
     * Returns unprocessed user attendance requests.
     * Duplicate requests from \Celesta\AdvancedGoogleCalendar\Model\EventAttendeeRepository::getUnprocessedByUserId
     * that are present in other than primary calendars are removed in favor to primary one so the user is able to
     * edit the event in case he has read only permission for the event, but calendar data in such requests
     * is substituted with data of other calendar to reflect original organizer
     * @return \Celesta\AdvancedGoogleCalendar\Model\EventAttendee[]
     * @todo use session to store notification content
     */
    private function getAttendees()
    {
        if ($this->attendees === null) {
            $this->attendees = [];
            try {
                $attendeesData = $this->eventAttendeeRepository->getUnprocessedByUserId($this->user->getId());
                if ($attendeesData) {
                    $inPrimaryCalendar = [];
                    $getHash = function ($data) {
                        return md5($data['event_id'] . $data['creator_email'] . $data['attendee_email']);
                    };
                    foreach ($attendeesData as $data) {
                        if ($data['is_primary']) {
                            $inPrimaryCalendar[$getHash($data)] = null;
                        }
                    }
                    foreach ($attendeesData as $data) {
                        if ($data['is_primary'] !== '1') {
                            $hash = $getHash($data);
                            if (array_key_exists($hash, $inPrimaryCalendar)) {
                                $inPrimaryCalendar[$hash] = ['summary' => $data['summary']];
                            }
                        }
                    }
                    foreach ($attendeesData as $data) {
                        $hash = $getHash($data);
                        if ($data['is_primary'] !== '1') {
                            if (array_key_exists($hash, $inPrimaryCalendar)) {
                                continue;
                            }
                        } else {
                            if (array_key_exists($hash, $inPrimaryCalendar)) {
                                $data['summary'] = $inPrimaryCalendar[$hash]['summary'];
                            }
                        }
                        // Checking that event has not passed
                        $event = $this->googleCalendar->getService()->events
                            ->get($data['calendar_google_id'], $data['event_id']);
                        if (!$this->eventDateTime->getHasEventPassed($event)) {
                            $this->attendees[] = $this->eventAttendeeRepository
                                ->getPopulated($data)
                                ->setData('calendar_summary', $data['summary'])
                                ->setData('event', $event);
                        }
                    }
                }
            } catch (\Exception $e) {
            }
        }

        return $this->attendees;
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $attendee
     * @return string
     */
    public function getActionContent(\Celesta\AdvancedGoogleCalendar\Model\EventAttendee $attendee)
    {
        /** @var \Google_Service_Calendar_Event $event */
        $event = $attendee->getData('event');
        /** @var \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar */
        $calendar = $this->user->getUserCalendarByGoogleId($event->getOrganizer()->getEmail());
        return $this->layout->createBlock(
            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\AttendeeAction::class,
            'attendee_action_block_' . $attendee->getEventHash(),
            ['event' => $event, 'calendar' => $calendar, 'attendee' => $attendee]
        )->toHtml();
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendee[] $attendees
     * @return string
     */
    public function getReplyContent(array $attendees)
    {
        $attendee = reset($attendees);
        /** @var \Google_Service_Calendar_Event $event */
        $event = $attendee->getData('event');
        /** @var \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar */
        $calendar = $this->user->getUserCalendarByGoogleId($event->getOrganizer()->getEmail());
        return $this->layout->createBlock(
            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\AttendeeReply::class,
            'attendee_reply_block_' . md5($attendee->getCalendarGoogleId() . $attendee->getEventId()),
            ['event' => $event, 'calendar' => $calendar, 'attendees' => $attendees]
        )->toHtml();
    }
}
