<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\System\Message;

class Configuration extends Base
{
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
     */
    protected $calendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     */
    public function __construct(
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        parent::__construct($authorization);
        $this->redirect = $redirect;
        $this->calendar = $calendar;
        $this->user = $user;
    }

    public function getIdentity()
    {
        $identity = null;
        if ($this->isAllowed()) {
            $identity = \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH . '/Configuration/' . $this->getAccessRequired($this->user);
        }

        return $identity;
    }

    public function isDisplayed()
    {
        return $this->isAllowed() && (bool)$this->getAccessRequired($this->user);
    }

    public function getText()
    {
        $level = $this->getAccessRequired($this->user);
        switch ($level) {
            case self::ACCESS_REQUIRED_LEVEL_ID:
                return __(
                    'Please add Google API authentication data to your Magento user ' .
                    '<a href="%1">Celesta configuration</a>.',
                    $this->user->getUserSettingsUrl()
                );
            case self::ACCESS_REQUIRED_LEVEL_AUTH:
                return __(
                    'To proceed with Celesta Advanced Google Calendar setup, please ' .
                    '<a href="%1">Authorize the Connection</a> to your Google account. On click you are redirected' .
                    ' to Google for the extension permission to access Google Calendars.',
                    $this->calendar->getAuthUrl()
                );
        }

        return null;
    }
}
