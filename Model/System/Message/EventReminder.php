<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\System\Message;

/**
 * Class reminds user about upcomming event by system message
 */
class EventReminder extends Base
{
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    private $layout;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\UserFactory
     */
    private $userFactory;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ReminderRepository
     */
    private $reminderRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository
     */
    private $calendarRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory
     */
    private $googleCalendarFactory;

    /**
     * @var array
     */
    private $remindersCalendars;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Session
     */
    private $session;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Reminder[]
     */
    private $reminders;

    /**
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\ReminderRepository $reminderRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory $googleCalendarFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\Session $session
     */
    public function __construct(
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Framework\View\LayoutInterface $layout,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory,
        \Celesta\AdvancedGoogleCalendar\Model\ReminderRepository $reminderRepository,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory $googleCalendarFactory,
        \Celesta\AdvancedGoogleCalendar\Model\Session $session
    ) {
        parent::__construct($authorization);
        $this->layout = $layout;
        $this->user = $user;
        $this->userFactory = $userFactory;
        $this->reminderRepository = $reminderRepository;
        $this->calendarRepository = $calendarRepository;
        $this->googleCalendarFactory = $googleCalendarFactory;
        $this->session = $session;
    }

    public function getIdentity()
    {
        $identity = null;
        if ($this->isAllowed()) {
            $ids = [];
            foreach ($this->getReminders() as $notification) {
                $ids[] = $notification->getId();
            }
            $identity = \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH . '/EventReminder/' . md5(implode('', $ids));
        }
        return $identity;
    }

    /**
     * @inheritdoc
     */
    public function isDisplayed()
    {
        return $this->isAllowed() && (bool)$this->getReminders();
    }

    /**
     * @inheritdoc
     * @todo: use session to store reminders content
     */
    public function getText()
    {
        $contents = [];
        $reminders = $this->getReminders();
        $calendars = $this->getRemindersCalendars($reminders);
        foreach ($reminders as $reminder) {
            try {
                $calendar = $this->calendarRepository->getPopulated($calendars[$reminder['calendar_id']]);
                /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
                $calendarUser = $this->userFactory->create(['userId' => $calendar->getUserId()]);
                /** @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar */
                $googleCalendar = $this->googleCalendarFactory->create(['user' => $calendarUser]);

                $event = $googleCalendar->getEventById($calendar->getGoogleId(), $reminder->getEventId());
                $contents[] = $this->getEventContent($event, $calendar);
            } catch (\Exception $e) {
            }
        }
        return implode('<hr class="celesta_event_hr">', $contents);
    }

    /**
     * @return \Celesta\AdvancedGoogleCalendar\Model\Reminder[]
     */
    private function getReminders()
    {
        if ($this->reminders === null) {
            $reminders = $this->reminderRepository->getActualReminders();

            $calendars = $this->getRemindersCalendars($reminders);
            $filteredReminders = [];
            foreach ($reminders as $reminder) {
                $reminderUserId = (int)$calendars[$reminder->getCalendarId()]['user_id'];
                if ($reminderUserId !== $this->user->getId()) { // if reminder calendar owner is another user
                    /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
                    $calendarUser = $this->userFactory->create(['userId' => $reminderUserId]);
                    if (!$this->isAllowedShared() || // if user has no shared reminders acl rule
                        $calendars[$reminder->getCalendarId()]['share_reminders'] !== '1' || // or calendar is no reminders sharind setting
                        $this->getAccessRequired($calendarUser) > self::ACCESS_REQUIRED_LEVEL_NONE) { // or reminder calendar owner has not set access creds of his google account
                        continue;
                    }
                }
                $filteredReminders[] = $reminder;
            }
            $this->reminders = $filteredReminders;
        }
        return $this->reminders;
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\Reminder[] $reminders
     * @return array[]
     */
    private function getRemindersCalendars($reminders)
    {
        if ($this->remindersCalendars === null) {
            $calendarIds = [];
            foreach ($reminders as $reminder) {
                $calendarIds[] = $reminder->getCalendarId();
            }
            $calendarIds = array_unique($calendarIds);
            $this->remindersCalendars = [];
            if ($calendarIds) {
                $this->remindersCalendars =
                    $this->calendarRepository->selectAllWhere(['calendar_id IN (?)' => $calendarIds]);
            }
        }
        return $this->remindersCalendars;
    }

    public function getEventContent(
        \Google_Service_Calendar_Event $event,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
    ) {
        return $this->layout->createBlock(
            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\EventDescription::class,
            'celesta_reminder_event_block' . md5($event->getOrganizer()->getEmail() . $event->getId() . self::class),
            ['calendar' => $calendar, 'event' => $event, 'data' => ['display_caption' => true]]
        )->toHtml();
    }
}
