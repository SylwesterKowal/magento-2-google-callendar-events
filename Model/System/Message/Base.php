<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model\System\Message;

class Base implements \Magento\Framework\Notification\MessageInterface
{
    const ACCESS_REQUIRED_LEVEL_NONE = 0;
    const ACCESS_REQUIRED_LEVEL_ID = 1;
    const ACCESS_REQUIRED_LEVEL_AUTH = 2;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    private $authorization;

    private static $isAllowed;
    private static $isAllowedShared;

    public function __construct(\Magento\Framework\AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    public function getIdentity()
    {
        return null;
    }

    public function isDisplayed()
    {
        return (bool)$this->isAllowed();
    }

    public function getText()
    {
        return null;
    }

    public function getSeverity()
    {
        return self::SEVERITY_NOTICE;
    }

    protected function isAllowed()
    {
        if (self::$isAllowed === null) {
            self::$isAllowed = $this->authorization
                ->isAllowed('Celesta_AdvancedGoogleCalendar::systemnotification');
        }
        return self::$isAllowed;
    }

    protected function isAllowedShared()
    {
        if (self::$isAllowedShared === null) {
            self::$isAllowedShared = $this->authorization
                ->isAllowed('Celesta_AdvancedGoogleCalendar::sharednotification');
        }
        return self::$isAllowedShared;
    }

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @return int
     * @throws \Celesta\AdvancedGoogleCalendar\Model\Exception
     */
    protected function getAccessRequired(\Celesta\AdvancedGoogleCalendar\Model\User $user)
    {
        $accessToken = $user->getAccessToken();
        $id = $user->getClientId();
        $secret = $user->getClientSecret();
        if (empty($accessToken)) {
            if (empty($id) || empty($secret)) {
                return self::ACCESS_REQUIRED_LEVEL_ID;
            }
            return self::ACCESS_REQUIRED_LEVEL_AUTH;
        }
        return self::ACCESS_REQUIRED_LEVEL_NONE;
    }
}
