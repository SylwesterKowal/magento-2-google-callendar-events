<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Celesta\AdvancedGoogleCalendar\Model;

use Celesta\AdvancedGoogleCalendar\Setup\InstallSchema;

/**
 * @param ResourceModel\Tag $resource
 */
class TagRepository extends AbstractRepository
{
    /**
     * @param ResourceModel\Tag $resource
     */
    public function __construct(
        ResourceModel\Tag $resource
    ) {
        parent::__construct($resource, 'tag');
    }

    public function getAllTags()
    {
        $select = $this->getConnection()->select();
        $select->from([$this->resource->getMainTable()]);

        return $this->getConnection()->fetchAssoc($select);
    }

    public function getIdsByTitles($tagsTitles)
    {
        $select = $this->getConnection()->select();
        $select
            ->from([$this->resource->getMainTable()], ['tag_id'])
            ->where('tag_title IN(?)', $tagsTitles);

        return $this->getConnection()->fetchCol($select);
    }

    public function getTitlesByIds($tagsIds)
    {
        $select = $this->getConnection()->select();
        $select
            ->from([$this->resource->getMainTable()], ['tag_title'])
            ->where('tag_id IN(?)', $tagsIds);

        return $this->getConnection()->fetchCol($select);
    }

    public function saveAllTitles($titles)
    {
        $allTagsTitles = array_column($this->getAllTags(), 'tag_title');
        $newTagsTitles = array_diff($titles, $allTagsTitles);
        $deleteTagsTitles = array_diff($allTagsTitles, $titles);
        $data = [];
        foreach ($newTagsTitles as $tagsTitle) {
            $data[] = ['tag_title' => $tagsTitle];
        }
        $connection = $this->getConnection();
        if ($newTagsTitles || $deleteTagsTitles) {
            $connection->beginTransaction();
        }
        if ($newTagsTitles) {
            $connection->insertMultiple($this->resource->getMainTable(), $data);
        }
        if ($deleteTagsTitles) {
            $connection->delete($this->resource->getMainTable(), ['tag_title IN (?)' => $deleteTagsTitles]);
        }
        if ($newTagsTitles || $deleteTagsTitles) {
            $connection->commit();
        }
    }

    /**
     * @param array $data
     * @return int The number of affected rows.
     */
    public function insertRowsToPivot($data)
    {
        return $this->getConnection()->insertMultiple(
            $this->resource->getTable(InstallSchema::CALENDAR_TAG_PIVOT_TABLE_NAME),
            $data
        );
    }

    /**
     * @param array $data
     * @return int The number of affected rows.
     */
    public function deleteRowsFromPivot($data)
    {
        return $this->getConnection()->delete(InstallSchema::CALENDAR_TAG_PIVOT_TABLE_NAME, $data);
    }

    /**
     * @param string[] $eventHashes
     * @param int[] $tagsIds
     * @return int[]
     * @throws \DomainException
     */
    public function getTagIdsByEventHashes(array $eventHashes, array $tagsIds = [])
    {
        $select = $this->getConnection()->select();
        $select
            ->from([$this->resource->getTable(InstallSchema::CALENDAR_TAG_PIVOT_TABLE_NAME)])
            ->where('event_hash IN (?)', $eventHashes);
        if ($tagsIds) {
            $select->where('tag_id IN (?)', $tagsIds);
        }

        return $this->getConnection()->fetchAssoc($select);
    }
}
