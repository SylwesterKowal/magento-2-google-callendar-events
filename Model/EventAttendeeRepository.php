<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

/**
 * @param ResourceModel\EventAttendee $resource
 */
class EventAttendeeRepository extends AbstractRepository
{
    /**
     * @var EventAttendeeFactory
     */
    private $eventAttendeeFactory;

    /**
     * @param ResourceModel\EventAttendee $resource
     */
    public function __construct(
        ResourceModel\EventAttendee $resource,
        EventAttendeeFactory $eventAttendeeFactory
    ) {
        parent::__construct($resource, 'event attendee');
        $this->eventAttendeeFactory = $eventAttendeeFactory;
    }

    /**
     * @param array $eventHashes
     * @return array[]
     */
    public function getAttendeesByEventHashes($eventHashes)
    {
        return $this->selectAllWhere(['event_hash IN (?)' => $eventHashes]);
    }

    /**
     * @param int $userId
     * @return array[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getUnprocessedByUserId($userId)
    {
        $calendarTable = $this->getConnection()->getTableName(
            \Celesta\AdvancedGoogleCalendar\Setup\InstallSchema::CALENDAR_CALENDAR_TABLE_NAME
        );
        $select = $this->getConnection()->select()
            ->from(['a' => $this->resource->getMainTable()], ['a.*', 'c.is_primary', 'c.summary'])
            ->joinInner(
                ['c' => $calendarTable],
                'a.creator_email = c.calendar_google_id AND a.response_status != \'needsAction\' OR ' .
                'a.attendee_email = c.calendar_google_id AND a.response_status = \'needsAction\''
            )
            ->joinInner(['c2' => $calendarTable], 'a.calendar_google_id = c2.calendar_google_id')
            ->where('a.addressee_status=0 AND c.user_id = :u AND c.is_primary=1 AND c2.user_id=:u AND c2.is_connected=1');

        return $this->getConnection()->fetchAssoc($select, ['u' => $userId]);
    }

    /**
     * @param string[] $data
     * @return EventAttendee
     */
    public function getPopulated(array $data)
    {
        /** @var EventAttendee $model */
        $model = $this->eventAttendeeFactory->create();
        $model->setId($data['event_attendee_hash']);
        $model->setEventHash($data['event_hash']);
        $model->setEventId($data['event_id']);
        $model->setCalendarGoogleId($data['calendar_google_id']);
        $model->setCreatorEmail($data['creator_email']);
        $model->setAttendeeEmail($data['attendee_email']);
        $model->setAttendeeDisplayName($data['attendee_display_name']);
        $model->setResponseStatus($data['response_status']);
        $model->setAdditionalGuests($data['additional_guests']);
        $model->setComment($data['comment']);
        $model->setAddresseeStatus($data['addressee_status']);

        return $model;
    }
}
