<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class ReminderHelper
{
    const TYPE_SYSTEM = 'system';

    const TYPE_GOOGLE_POPUP = 'popup';

    const TYPE_GOOGLE_EMAIL = 'email';

    const BEFORE_UNIT_MINUTE = 1;

    const BEFORE_UNIT_HOUR = 2;

    const BEFORE_UNIT_DAY = 3;

    const BEFORE_UNIT_WEEK = 4;

    const GOOGLE_CALENDAR_SYSTEM_KEY = 'systemReminder'; // Value stored in minutes (to event start)

    /**
     * @param array $reminders
     * @return array
     */
    public function getSystemReminders(array $reminders)
    {
        return $this->filterReminders($reminders, 'type', [self::TYPE_SYSTEM]);
    }

    /**
     * @param array $reminders
     * @return array
     */
    public function getGoogleReminders(array $reminders)
    {
        return $this->filterReminders($reminders, 'type', [self::TYPE_GOOGLE_POPUP, self::TYPE_GOOGLE_EMAIL]);
    }

    /**
     * @param array $reminders
     * @param string $key
     * @param array $values
     * @return array
     */
    public function filterReminders(array $reminders, $key, $values)
    {
        return array_filter($reminders, function ($reminder) use ($key, $values) {
            return in_array($reminder[$key], $values, false);
        });
    }

    /**
     * @param integer $minutes
     * @return array
     */
    public function getBeforeUnits($minutes)
    {
        if ($minutes % 60 === 0) {
            $value = $minutes / 60;
            $unit = self::BEFORE_UNIT_HOUR;
            if ($value % 24 === 0) {
                $value /= 24;
                $unit = self::BEFORE_UNIT_DAY;
                if ($value % 7 === 0) {
                    $value /= 7;
                    $unit = self::BEFORE_UNIT_WEEK;
                }
            }
        } else {
            $value = $minutes;
            $unit = self::BEFORE_UNIT_MINUTE;
        }

        return [$value, $unit];
    }

    /**
     * @param integer $beforeValue
     * @param integer $beforeUnit
     * @return integer
     */
    public function getBeforeMinutes($beforeValue, $beforeUnit)
    {
        switch ($beforeUnit) {
            case self::BEFORE_UNIT_MINUTE:
                return $beforeValue;
            case self::BEFORE_UNIT_HOUR:
                return $beforeValue * 60;
            case self::BEFORE_UNIT_DAY:
                return $beforeValue * 1440;
            case self::BEFORE_UNIT_WEEK:
            default:
                return $beforeValue * 10080;
        }
    }

    /**
     * @param integer $beforeValue
     * @param integer $beforeUnit
     * @return bool
     */
    public function validateBeforeValue($beforeValue, $beforeUnit)
    {
        return $this->getBeforeMinutes($beforeValue, $beforeUnit) <= 40320; // 4 weeks in minutes;
    }

    /**
     * @param array $reminders
     * @return array
     */
    public function normalize(array $reminders)
    {
        $normalized = [];
        foreach ($reminders as $reminder) {
            $type = $reminder['reminder_type'];
            $before_value = (int)$reminder['before_value'];
            $before_unit = (int)$reminder['before_unit'];
            $beforeIsValid = $this->validateBeforeValue($before_value, $before_unit);
            if ($type && $beforeIsValid && $before_value && $before_unit) {
                $before_minutes = $this->getBeforeMinutes($before_value, $before_unit);
                $normalized[] = compact('type', 'before_value', 'before_unit', 'before_minutes');
            }
        }

        return $this->filterReminders($normalized, 'type', [
            self::TYPE_SYSTEM, self::TYPE_GOOGLE_POPUP, self::TYPE_GOOGLE_EMAIL
        ]);
    }
}
