<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Magento\Framework\App\Cache\Type\Config;

class GoogleCalendar
{
    const CONFIG_PARAM_EVENTS_NEXT_SYNC_TOKEN = 'events_next_sync_token';

    const CONFIG_PARAM_CALENDARS_NEXT_SYNC_TOKEN = 'calendars_next_sync_token';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    private $configWriter;

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    private $cacheTypeList;

    /**
     * @var EventPersistence
     */
    private $eventPersistence;

    /**
     * @var \Google_Client
     */
    private $client;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CalendarRepository
     */
    private $calendarRepository;

    /**
     * @var array
     */
    private $loadedEntities = [];

    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\Model\Auth\Session $authSession,
        User $user,
        EventPersistence $eventPersistence,
        CalendarRepository $calendarRepository
    ) {
        $this->url = $url;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
        $this->eventPersistence = $eventPersistence;
        $this->user = $user;
        $this->calendarRepository = $calendarRepository;
    }

    /**
     * @param \Google_Service_Calendar_Event $event
     * @param string $calendarId
     * @return \Google_Service_Calendar_Event
     */
    public function saveEvent(\Google_Service_Calendar_Event $event, $calendarId)
    {
        $service = $this->getService();
        if ($event->getId()) {
            $event = $service->events->update($calendarId, $event->getId(), $event);
        } else {
            $event = $service->events->insert($calendarId, $event);
        }
        $this->loadedEntities[md5($calendarId . $event->getId())] = $event;

        return $event;
    }

    /**
     * @param string $eventId
     * @param string $originalCalendarId
     * @param string $destinationCalendarId
     * @return \Google_Service_Calendar_Event
     */
    public function moveEvent($eventId, $originalCalendarId, $destinationCalendarId)
    {
        $this->loadedEntities[md5($originalCalendarId . $eventId)] = null;

        return $this->getService()->events->move($originalCalendarId, $eventId, $destinationCalendarId);
    }

    /**
     * @param string $calendarId
     * @param string $eventId
     * @return \Google_Service_Calendar_Event
     */
    public function getEventById($calendarId, $eventId)
    {
        $key = md5($calendarId . $eventId);
        if (empty($this->loadedEntities[$key])) {
            $this->loadedEntities[$key] = $this->getService()->events->get($calendarId, $eventId);
        }

        return $this->loadedEntities[$key];
    }

    /**
     * @param string $calendarId
     * @param string $eventId
     * @param \Google_Service_Calendar_Event $event
     * @return \Google_Service_Calendar_Event
     */
    public function getUpdateEvent($calendarId, $eventId, \Google_Service_Calendar_Event $event)
    {
        $this->loadedEntities[md5($calendarId . $eventId)] = null;
        return $this->getService()->events->update($calendarId, $eventId, $event);
    }

    /**
     * @param string $calendarId
     * @param array $eventIds
     * @return array|null
     * @throws \Exception
     */
    public function deleteEventsById($calendarId, $eventIds)
    {
        $batch = $this->getBatch();
        foreach ($eventIds as $eventId) {
            $service = $this->getService();
            /** @var \Psr\Http\Message\RequestInterface $request */
            $request = $service->events->delete($calendarId, $eventId);
            $batch->add($request, $eventId);
            $this->loadedEntities[md5($calendarId . $eventId)] = null;
        }

        return $batch->execute();
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->getBareClient()->createAuthUrl();
    }

    /**
     * @return \Google_Service_Calendar
     */
    public function getService()
    {
        return new \Google_Service_Calendar($this->getClient());
    }

    /**
     * @return \Google_Http_Batch
     * @throws \Exception
     */
    public function getBatch()
    {
        $client = $this->getClient();
        $client->setUseBatch(true);

        return new \Google_Http_Batch($client);
    }

    /**
     * @return \Google_Client
     * @throws \Celesta\AdvancedGoogleCalendar\Model\Exception
     */
    public function getClient()
    {
        if ($this->client === null) {
            $client = $this->getBareClient();
            $accessToken = $this->user->getAccessToken();
            if ($accessToken) {
                $client->setAccessToken($accessToken);
            }
            if ($client->isAccessTokenExpired()) {
                try {
                    $refreshToken = $this->user->getAccessToken('refresh_token');
                } catch (\Exception $e) {
                    if ($e->getCode() === \Celesta\AdvancedGoogleCalendar\Model\Exception::REASON_GOOGLE_REFRESH_TOKEN_IS_MISSING) {
                        if (!$this->user->getAccessToken()) {
                            throw new \Celesta\AdvancedGoogleCalendar\Model\Exception(
                                __('Google Access token is missing'),
                                null,
                                \Celesta\AdvancedGoogleCalendar\Model\Exception::REASON_GOOGLE_ACCESS_TOKEN_IS_MISSING
                            );
                        }
                    }
                    throw $e;
                }
                $client->setApprovalPrompt('force');
                $client->fetchAccessTokenWithRefreshToken($refreshToken);
                $this->user->saveAccessToken(json_encode($client->getAccessToken()));
            }
            $this->client = $client;
        }

        return $this->client;
    }

    /**
     * @return \Google_Client
     * @throws \Celesta\AdvancedGoogleCalendar\Model\Exception
     */
    private function getBareClient()
    {
        $clientId = $this->user->getClientId();
        $clientSecret = $this->user->getClientSecret();
        if (!$clientId || !$clientSecret) {
            throw new \Celesta\AdvancedGoogleCalendar\Model\Exception(
                __('Google API authentication data is missing'),
                null,
                Exception::REASON_GOOGLE_API_AUTH_DATA_IS_MISSING
            );
        }
        $bareClient = new \Google_Client();
        $bareClient->setApplicationName('Magento eShop');
        $bareClient->setClientId($clientId);
        $bareClient->setClientSecret($clientSecret);
        $bareClient->setScopes([\Google_Service_Calendar::CALENDAR]);
        $bareClient->setAccessType('offline');
        $bareClient->setPrompt('consent');
        $bareClient->setIncludeGrantedScopes(true);
        $bareClient->setRedirectUri($this->url->getUrl(\Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'oauth'));

        return $bareClient;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function syncCalendarsAndEvents()
    {
        $this->syncCalendars();
        $connectedCalendars = $this->user->getConnectedCalendars();
        foreach ($connectedCalendars as $calendar) {
            try {
                $this->syncCalendarEvents(
                    $calendar->getGoogleId(),
                    $this->scopeConfig->getValue($this->getConfigEventsNextSyncTokenPath($calendar->getId()))
                );
            } catch (\Google_Service_Exception $e) {
                if ($e->getCode() === 410) {
                    $this->configWriter->delete($this->getConfigEventsNextSyncTokenPath($calendar->getId()));
                    $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
                    $this->syncCalendarEvents($calendar->getGoogleId());
                }
            }
        }
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function syncCalendars()
    {
        $calendarsToUpdate = [];
        $calendarsDataToInsert = [];
        $calendars = $this->user->getCalendars(true);
        $listEntries = $this->getCalendarListEntries();

        foreach ($listEntries as $listEntry) {
            $found = false;
            foreach ($calendars as $i => $calendar) {
                if ($calendar->getGoogleId() === $listEntry->getId()) {
                    if ($calendar->getEtag() !== $listEntry->getEtag() ||
                        $calendar->getTimeZone() !== $listEntry->getTimeZone() // Google bug: changing timezone does not changes etag
                    ) {
                        $calendar->setSummary($listEntry->getSummary());
                        $calendar->setTimeZone($listEntry->getTimeZone());
                        $calendar->setEtag($listEntry->getEtag());
                        $calendar->setDescription($listEntry->getDescription());
                        $calendarsToUpdate[] = $calendar;
                    }
                    $found = true;
                    unset($calendars[$i]);
                    break;
                }
            }
            if ($found === false) {
                $calendarsDataToInsert[] = [
                    'calendar_google_id' => $listEntry->getId(),
                    'user_id' => $this->user->getId(),
                    'summary' => $listEntry->getSummary(),
                    'time_zone' => $listEntry->getTimeZone(),
                    'use_system_time_zone' => 1,
                    'is_connected' => $listEntry->getPrimary() ? 1 : 0,
                    'is_dashboard' => $listEntry->getPrimary() ? 1 : 0,
                    'share_reminders' => 0,
                    'is_primary' => $listEntry->getPrimary() ? 1 : 0,
                    'etag' => $listEntry->getEtag()
                ];
            }
        }
        if ($calendarsToUpdate) {
            foreach ($calendarsToUpdate as $calendar) {
                $this->calendarRepository->save($calendar);
            }
        }
        if ($calendarsDataToInsert) {
            $this->calendarRepository->insertRows($calendarsDataToInsert);
        }
        if ($calendars) {
            $this->calendarRepository->deleteRows(['calendar_id IN (?)' => array_map(function (Calendar $c) {
                return $c->getId();
            }, $calendars)]);
        }
    }

    /**
     * @param string $calendarGoogleId
     * @param string|null $syncToken
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function syncCalendarEvents($calendarGoogleId, $syncToken = null)
    {
        $nextPageToken = null;
        $nextSyncToken = null;
        $googleEventArrays = [];
        do {
            $optParams = [];
            if ($nextPageToken) {
                $optParams['pageToken'] = $nextPageToken;
            } elseif ($syncToken) {
                $optParams['syncToken'] = $syncToken;
            }
            $googleEvents = $this->getService()->events->listEvents($calendarGoogleId, $optParams);
            $googleEvent = $googleEvents->getItems();
            if ($googleEvent) {
                $googleEventArrays[] = $googleEvent;
                $nextPageToken = $googleEvents->getNextPageToken();
                $nextSyncToken = $googleEvents->getNextSyncToken();
            }
        } while ($nextPageToken);

        if ($googleEventArrays) {
            $calendar = $this->user->getUserCalendarByGoogleId($calendarGoogleId);
            $tokenUpdate = function () {
            };
            if ($nextSyncToken) {
                $configWriter = $this->configWriter;
                $syncTokenPath = $this->getConfigEventsNextSyncTokenPath($calendar->getId());
                $cacheTypeList = $this->cacheTypeList;
                $tokenUpdate = function () use ($configWriter, $syncTokenPath, $nextSyncToken, $cacheTypeList) {
                    $configWriter->save($syncTokenPath, $nextSyncToken);
                    $cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
                };
            }
            $this->eventPersistence->syncCalendarEvents($calendar, $googleEventArrays, $tokenUpdate);
        }
    }

    public function getAccessToken($authCode)
    {
        return $this->getBareClient()->fetchAccessTokenWithAuthCode($authCode);
    }

    /**
     * @see https://developers.google.com/google-apps/calendar/v3/reference/calendarList/list
     * @see also Calendars vs CalendarList, https://developers.google.com/calendar/concepts/events-calendars#calendar_and_calendar_list
     * @param array $optParams
     * @return \Google_Service_Calendar_CalendarListEntry[]
     * @throws \Exception
     */
    public function getCalendarListEntries(array $optParams = [])
    {
        if (empty($optParams['maxResults'])) {
            $optParams['maxResults'] = 200;
        }
        if (empty($optParams['minAccessRole'])) {
            $optParams['minAccessRole'] = 'reader';
        }
        $key = md5('calendarListEntries' . json_encode($optParams));
        if (!array_key_exists($key, $this->loadedEntities)) {
            $this->loadedEntities[$key] = [];
            do {
                $calendarList = $this->getService()->calendarList->listCalendarList($optParams);
                foreach ($calendarList->getItems() as $item) {
                    $this->loadedEntities[$key][] = $item;
                }
            } while ($optParams['pageToken'] = $calendarList->getNextPageToken());
        }

        return $this->loadedEntities[$key];
    }

    /**
     * @see Calendars vs CalendarList, https://developers.google.com/calendar/concepts/events-calendars#calendar_and_calendar_list
     * @param string $calendarGoogleId
     * @return \Google_Service_Calendar_CalendarListEntry
     */
    public function getCalendarById($calendarGoogleId)
    {
        $key = md5('calendarEntity' . $calendarGoogleId);
        if (empty($this->loadedEntities[$key])) {
//            $this->loadedEntities[$key] = $this->getService()->calendars->get($calendarGoogleId);
            $this->loadedEntities[$key] = $this->getService()->calendarList->get($calendarGoogleId);
        }

        return $this->loadedEntities[$key];
    }

    /**
     * @see Calendars vs CalendarList, https://developers.google.com/calendar/concepts/events-calendars#calendar_and_calendar_list
     * @param string $calendarGoogleId
     * @param \Google_Service_Calendar_CalendarListEntry $calendarListEntry
     * @return \Google_Service_Calendar_CalendarListEntry
     */
    public function updateCalendarById($calendarGoogleId, $calendarListEntry)
    {
        $calendar = $this->getService()->calendars->get($calendarGoogleId);
        $calendar->setSummary($calendarListEntry->getSummary());
        $calendar->setDescription($calendarListEntry->getDescription());
        $calendar->setTimeZone($calendarListEntry->getTimeZone());
        $updatedCalendar = $this->getService()->calendars->update($calendarGoogleId, $calendar);

        $key = md5('calendarEntity' . $calendarGoogleId);
        $this->loadedEntities[$key] = null;
        if ($updatedCalendar && $updatedCalendar->getSummary() != $calendarListEntry->getSummaryOverride()) {
            $calendarListEntry->setSummaryOverride($calendarListEntry->getSummary());
            $updatedCalendarListEntry = $this->getService()->calendarList->update($calendarGoogleId, $calendarListEntry);
            $this->loadedEntities[$key] = $updatedCalendarListEntry;
        }
        return $updatedCalendarListEntry;
    }

    public function getConfigEventsNextSyncTokenPath($calendarId)
    {
        return Module::CONFIG_PATH . '/' . self::CONFIG_PARAM_EVENTS_NEXT_SYNC_TOKEN . '/user-' .
            $this->user->getId() . '/calendar-' . $calendarId;
    }
}
