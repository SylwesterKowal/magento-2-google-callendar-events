<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;

class AbstractRepository
{
    /**
     * @var string
     */
    protected $entityName;

    /**
     * @var AbstractDb
     */
    protected $resource;

    /**
     * @param AbstractDb $resource
     * @param null $entityName
     */
    public function __construct(
        AbstractDb $resource,
        $entityName = null
    ) {
        $this->resource = $resource;
        $this->entityName = $entityName;
    }

    /**
     * @return false|\Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        return $this->resource->getConnection();
    }

    /**
     * @param AbstractModel $model
     * @return AbstractModel
     * @throws CouldNotSaveException
     */
    public function save(AbstractModel $model)
    {
        try {
            $this->resource->save($model);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to save ' . $this->entityName), $e);
        }

        return $model;
    }

    /**
     * @param AbstractModel $model
     * @throws CouldNotDeleteException
     */
    public function delete(AbstractModel $model)
    {
        try {
            $this->resource->delete($model);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(
                __('Unable to delete %1 with ID %2', $this->entityName, $model->getId()),
                $e
            );
        }
    }

    /**
     * @param array $where
     * @return array
     */
    public function selectAllWhere(array $where)
    {
        $select = $this->getConnection()->select();
        $select->from($this->resource->getMainTable());
        foreach ($where as $condition => $value) {
            $select->where($condition, $value);
        }

        return $this->getConnection()->fetchAssoc($select);
    }

    /**
     * @param int|string $idFieldValue
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getRowById($idFieldValue)
    {
        return $this->getConnection()->fetchRow(
            $this->getConnection()->select()
                ->from($this->resource->getMainTable())
                ->where($this->resource->getIdFieldName() . ' = ?', $idFieldValue)
        );
    }

    /**
     * @param array $data Column-value pairs or array of Column-value pairs
     * @return int The number of affected rows
     * @throws CouldNotSaveException
     */
    public function insertRows(array $data)
    {
        try {
            return $this->getConnection()->insertMultiple($this->resource->getMainTable(), $data);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to save %1s', $this->entityName), $e);
        }
    }

    /**
     * @param array $bind Column-value pairs
     * @param mixed $where UPDATE WHERE clause(s)
     * @return int The number of affected rows
     * @throws CouldNotSaveException
     */
    public function updateRows(array $bind, $where)
    {
        try {
            return $this->getConnection()->update($this->resource->getMainTable(), $bind, $where);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to update %1s', $this->entityName), $e);
        }
    }

    /**
     * @param array $data
     * @return int The number of affected rows.
     * @throws CouldNotDeleteException
     */
    public function deleteRows(array $data)
    {
        try {
            return $this->getConnection()->delete($this->resource->getMainTable(), $data);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Cannot delete %1s', $this->entityName), $e);
        }
    }
}
