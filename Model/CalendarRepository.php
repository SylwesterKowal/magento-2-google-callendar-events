<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

/**
 * @param ResourceModel\Calendar $resource
 */
class CalendarRepository extends AbstractRepository
{
    /**
     * @var CalendarFactory
     */
    private $calendarFactory;

    /**
     * @param ResourceModel\Calendar $resource
     * @param CalendarFactory $calendarFactory
     */
    public function __construct(
        ResourceModel\Calendar $resource,
        CalendarFactory $calendarFactory
    ) {
        parent::__construct($resource, 'calendar');
        $this->calendarFactory = $calendarFactory;
    }

    /**
     * @param $userId
     * @return Calendar[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCalendarsByUserId($userId)
    {
        $select = $this->getConnection()->select();
        $select
            ->from($this->resource->getMainTable())
            ->where('user_id = ?', $userId);

        $calendars = [];
        $result = $this->getConnection()->fetchAll($select);
        foreach ($result as $item) {
            $calendars[] = $this->getPopulated($item);
        }
        return $calendars;
    }

    /**
     * @param $userId
     * @return Calendar|null
     * @deprecated
     */
    public function getPrimaryByUserId($userId)
    {
        $select = $this->getConnection()->select();
        $select
            ->from($this->resource->getMainTable())
            ->where('user_id = ?', $userId)
            ->where('is_primary = 1')
            ->limit(1);
        $result = $this->getConnection()->fetchRow($select);

        return $result ? $this->getPopulated($result) : null;
    }

    /**
     * @param string[] $item
     * @return Calendar
     */
    public function getPopulated(array $item)
    {
        return $this->calendarFactory->create()
            ->setId($item[Calendar::KEY_ID])
            ->setGoogleId($item[Calendar::KEY_GOOGLE_ID])
            ->setUserId($item[Calendar::KEY_USER_ID])
            ->setSummary($item[Calendar::KEY_SUMMARY])
            ->setIsPrimary($item[Calendar::KEY_IS_PRIMARY])
            ->setTimeZone($item[Calendar::KEY_TIME_ZONE])
            ->setUseSystemTimeZone($item[Calendar::KEY_USE_SYSTEM_TIME_ZONE])
            ->setIsConnected($item[Calendar::KEY_IS_CONNECTED])
            ->setIsDashboard($item[Calendar::KEY_IS_DASHBOARD])
            ->setShareReminders($item[Calendar::KEY_SHARE_REMINDERS])
            ->setDescription($item[Calendar::KEY_DESCRIPTION])
            ->setEtag($item[Calendar::KEY_ETAG]);
    }
}
