<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Model;

class Exception extends \Magento\Framework\Exception\LocalizedException
{
    const REASON_GOOGLE_API_AUTH_DATA_IS_MISSING = 700;
    const REASON_GOOGLE_ACCESS_TOKEN_IS_MISSING = 701;
    const REASON_GOOGLE_REFRESH_TOKEN_IS_MISSING = 702;
}
