/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/form/element/date'
], function (Element) {
    'use strict';

    return Element.extend({
        defaults: {
            valuesForOptions: [],
            systemState: false,
            emailState: false,
            inverseVisibility: false,
            imports: {
                systemChange: '${ $.parentName }.is_system:checked',
                emailChange: '${ $.parentName }.is_email:checked'
            }
        },

        initialize: function () {
            this._super();
            return this;
        },

        systemChange: function (checked) {
            this.systemState = checked;
            this.toggleVisibility();
        },

        emailChange: function (checked) {
            this.emailState = checked;
            this.toggleVisibility();
        },

        /**
         * Toggle visibility state.
         *
         * @param {Number} selected
         */
        toggleVisibility: function () {
            this.disabled(!this.systemState && !this.emailState);
        }
    });
});
