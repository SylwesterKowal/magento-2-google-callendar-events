/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/form/element/single-checkbox'
], function (Element) {
    'use strict';
    return Element.extend({
        defaults: {
            imports: {
                guestChange: '${ $.parentName }.guests_modify_event:checked'
            }
        },
        guestChange: function (checked) {
            if (checked) {
                this.checked(checked);
            }
        }
    });
});
