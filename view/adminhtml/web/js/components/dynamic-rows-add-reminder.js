/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/dynamic-rows/dynamic-rows'
], function (Element) {
    'use strict';

    return Element.extend({

        defaults: {},

        initialize: function () {
            this._super();
        },

        deleteRecord: function () {
            this._super();
            if (this.getRecordCount() < 5) {
                this.setVisibility("visible");
            }
        },

        addChild: function () {
            this._super();
            if (this.getRecordCount() > 3) {
                this.setVisibility("hidden");
            }
        },

        setVisibility: function (visibility) {
            document.querySelector("table.admin__dynamic-rows tfoot").style.visibility = visibility;
        }
    });
});
