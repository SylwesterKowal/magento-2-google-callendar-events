/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'jquery',
    'moment',
    'Magento_Ui/js/form/element/date'
], function ($, moment, Element) {
    'use strict';

    function getFieldName(suffix)
    {
        return $('input[name="general[event_date_time][date_' + suffix + ']"]');
    }

    return Element.extend({
        onValueChange: function (value) {
            var dp, secondDate;
            this._super(value);
            if (value) {
                value = moment(value, this.outputDateFormat).toDate();
                dp = getFieldName(this.suffix === 'to' ? 'from' : 'to');
                secondDate = dp.datepicker('getDate');
                dp.datepicker('option', this.suffix === 'to' ? 'maxDate' : 'minDate', value);
                if (this.suffix === 'from' && value > secondDate || this.suffix === 'to' && value < secondDate) {
                    dp.trigger('change');
                }
            }
        }
    });
});
