/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/form/form'
], function (Form) {
    'use strict';
    return Form.extend({
        redirectTo: function (alias) {
            this.setAdditionalData({back_alias: alias}).save();
        }
    });
});
