/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/form/element/date'
], function (Element) {
    'use strict';

    return Element.extend({
        defaults: {
            options: {
                timeFormat: 'HH:mm'
            },
            imports: {
                storeTimeZone: '${ $.provider }:data.event_date_time.time_zone',
            }
        },

        onValueChange: function (value) {
            if (value.indexOf('T') > 0) {
                this.initialValue = '-';
                this._super(value);
            } else if (value !== this.shiftedValue()) {
                this.shiftedValue(value);
            }
        },

        onShiftedValueChange: function (value) {
            if (value.indexOf('T') > 0) {
                this._super(value);
            } else if (value !== this.value()) {
                this.value(value);
            }
        }
    });
});
