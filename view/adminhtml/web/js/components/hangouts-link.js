/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'Magento_Ui/js/form/element/abstract'
], function (Element) {
    'use strict';
    return Element.extend({
        defaults: {
            imports: {
                visibilityChange: '${ $.parentName }.hangouts:checked'
            }
        },
        visibilityChange: function (checked) {
            this.visible(checked && this.value());
        }
    });
});
