<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Cron;

class CalendarSynchronizer
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory
     */
    private $calendarFactory;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\UserFactory
     */
    private $userFactory;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory $calendarFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory $calendarFactory,
        \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory
    ) {
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->calendarFactory = $calendarFactory;
        $this->userFactory = $userFactory;
    }

    /**
     * Synchronizes calendars with Google
     *
     * @return void
     */
    public function execute()
    {
        $syncFreqMin = (int)$this->scopeConfig->getValue(
            \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH_CRON_SYNC_MIN
        );
        $curMin = floor((time() - strtotime('today')) / 60);
        if ($curMin === 0) {
            $curMin = 60;
        }
        if ($syncFreqMin === 0 || $curMin % $syncFreqMin !== 0) {
            return;
        }
        $calendarUsers = $this->scopeConfig->getValue(
            \Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH . '/user_ids'
        );
        $calendarUsers = $calendarUsers ? explode(',', $calendarUsers) : [];
        foreach ($calendarUsers as $userId) {
            /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
            $calendarUser = $this->userFactory->create(['userId' => $userId]);
            /** @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar */
            $calendar = $this->calendarFactory->create(['user' => $calendarUser]);
            try {
                $calendar->syncCalendarsAndEvents();
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }
}
