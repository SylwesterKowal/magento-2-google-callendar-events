<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\Component;

class TagOptionSource implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag
     */
    private $resource;

    /**
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $resource
     */
    public function __construct(\Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $resource)
    {
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray()
    {
        $connection = $this->resource->getConnection();
        $select = $connection->select();
        $select->from(['t' => $this->resource->getMainTable()], ['tag_id AS value', 'tag_title AS label']);

        return $connection->fetchAssoc($select);
    }
}
