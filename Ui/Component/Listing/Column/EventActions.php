<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\Component\Listing\Column;

class EventActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /** @var \Magento\Framework\UrlInterface */
    private $urlBuilder;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $htmlLink = $item['html_link'];
                $parts = parse_url($htmlLink);
                parse_str($parts['query'], $params);
                if (!empty($params['eid'])) {
                    $htmlLink = 'https://calendar.google.com/calendar/r/eventedit/' . $params['eid'];
                }

                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        \Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'event/edit',
                        ['calendar' => $item['calendar_id'], 'event' => $item['event_id']]
                    ),
                    'label' => __('Edit')
                ];
                $item[$this->getData('name')]['edit_google'] = [
                    'href' => $htmlLink,
                    'label' => __('Edit in Google')
                ];
                $item[$this->getData('name')]['print'] = [
                    'href' => sprintf(
                        'https://calendar.google.com/calendar/printevent?eid=%s&sf=true&pjs=true',
                        $params['eid']
                    ),
                    'label' => __('Print')
                ];
            }
        }

        return $dataSource;
    }
}
