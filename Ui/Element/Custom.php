<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\Element;

class Custom extends \Magento\Framework\Data\Form\Element\AbstractElement
{
    /**
     * Retrieve Element HTML
     * @return string
     */
    public function getElementHtml()
    {
        return $this->getData('html');
    }
}
