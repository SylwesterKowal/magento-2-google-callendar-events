<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\DataProvider;

use Magento\Framework\Exception\LocalizedException;
use Celesta\AdvancedGoogleCalendar\Model\ReminderHelper;

class CalendarFormDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var ReminderHelper
     */
    private $reminderHelper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory
     */
    protected $collection;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper
     */
    private $eventMapper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\TagRepository
     */
    private $tagRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventDateTime
     */
    private $eventDate;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository
     */
    private $calendarRepository;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDate
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar
     * @param ReminderHelper $reminderHelper
     * @param \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $eventsCollectionFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resource,
        \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDate,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar,
        ReminderHelper $reminderHelper,
        \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $eventsCollectionFactory,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarRepository $calendarRepository,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, 'calendar', $meta, $data);
        $this->request = $request;
        $this->resource = $resource;
        $this->googleCalendar = $googleCalendar;
        $this->reminderHelper = $reminderHelper;
        $this->eventMapper = $eventMapper;
        $this->collection = $eventsCollectionFactory->create();
        $this->user = $user;
        $this->tagRepository = $tagRepository;
        $this->eventDate = $eventDate;
        $this->calendarRepository = $calendarRepository;
    }

    public function getRequestFieldName()
    {
        return $this->requestFieldName;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getData()
    {
        $calendarId = $this->request->getParam('calendar');
        $calendarData = $this->calendarRepository->getRowById($calendarId);
        $calendar = $this->calendarRepository->getPopulated($calendarData);
        $isPrimary = (bool)$calendar->getIsPrimary();
        $this->data[$calendar->getId()] = [
            'calendar_id' => $calendar->getId(),
            'calendar_google_id' => $calendar->getGoogleId(),
            'summary' => $calendar->getSummary(),
            'is_primary' => $isPrimary,
            'is_primary_label' => __($isPrimary ? 'yes' : 'no'),
            'time_zone' => $calendar->getTimeZone(),
            'use_system_time_zone' => (string)$calendar->getUseSystemTimeZone(),
            'is_connected' => (string)$calendar->getIsConnected(),
            'is_dashboard' => (string)$calendar->getIsDashboard(),
            'share_reminders' => (string)$calendar->getShareReminders(),
            'description' => $calendar->getDescription()
        ];
        return $this->data;
    }
}
