<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\DataProvider;

class CalendarGridDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory
     */
    private $calendarTimeZoneFactory;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarFactory
     */
    private $calendarFactory;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Calendar\CollectionFactory $collectionFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarFactory $calendarFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Calendar\CollectionFactory $collectionFactory,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarFactory $calendarFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->calendarTimeZoneFactory = $calendarTimeZoneFactory;
        $this->calendarFactory = $calendarFactory;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $boolMapper = function ($v) {
            return __($v ? 'yes' : 'no');
        };
        $calendars = $this->getCollection()->toArray();
        if (!empty($calendars['items']) && is_array($calendars['items'])) {
            foreach ($calendars['items'] as &$calendar) {
                $calendar['is_connected'] = $boolMapper($calendar['is_connected']);
                $calendar['is_dashboard'] = $boolMapper($calendar['is_dashboard']);
                $calendar['share_reminders'] = $boolMapper($calendar['share_reminders']);
                if ($calendar['use_system_time_zone']) {
                    /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarFactory */
                    $calendarModel = $this->calendarFactory->create(['data' => $calendar]);
                    /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZone $calendarTimeZone */
                    $calendarTimeZone = $this->calendarTimeZoneFactory->create(['calendar' => $calendarModel]);
                    $calendar['time_zone'] = $calendarTimeZone->getTimezone() . ' (System)';
                }
            }
        }
        return $calendars;
    }
}
