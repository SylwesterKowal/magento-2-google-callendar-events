<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\DataProvider;

use Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper;

class EventGridDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory
     */
    private $calendarTimeZoneFactory;

    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    private $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    private $addFilterStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    private $valueMapper;

    /**
     * @var \DateTimeZone[]
     */
    private $calendarsTimeZones;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $collectionFactory
     * @param EventValueMapper $valueMapper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $collectionFactory,
        \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $valueMapper,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->user = $user;
        $this->calendarTimeZoneFactory = $calendarTimeZoneFactory;
        $this->collection = $collectionFactory->create();
        $this->valueMapper = $valueMapper;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $events = $this->getCollection()->toArray();
        foreach ($events['items'] as &$event) {
            if (empty($event['summary'])) {
                $event['summary'] = '(' . __('No title') . ')';
            }
            $event['visibility'] = $this->valueMapper->getVisibilityLabel($event['visibility']);
            $event['transparency'] = $this->valueMapper->getTransparencyLabel($event['transparency']);
            $event['recurrence'] = $this->valueMapper->getRecurrenceLabel($event['recurrence']);

            $isAllDay = (bool)$event['all_day'];
            $event['all_day'] = $isAllDay ? __('yes') : __('no');
            $start = new \DateTime($event['start_utc'], new \DateTimeZone('UTC'));
            $end = new \DateTime($event['end_utc'], new \DateTimeZone('UTC'));
            if ($isAllDay) {
                $event['start_utc'] = $start->format('Y-m-d');
                $event['end_utc'] = $end->modify('-1 days')->format('Y-m-d');
            } else {
                $timezone = $this->getCalendarTimeZone($event['calendar_id']);
                $event['start_utc'] = $start->setTimezone($timezone)->format('Y-m-d, H:i') . ' (' . $timezone->getName() . ')';
                $event['end_utc'] = $end->setTimezone($timezone)->format('Y-m-d, H:i') . ' (' . $timezone->getName() . ')';
            }
        }
        return $events;
    }

    private function getCalendarTimeZone($calendarId)
    {
        if (!isset($this->calendarsTimeZones[$calendarId])) {
            $calendar = $this->user->getUserCalendarById($calendarId);
            /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZone $calendarTimeZone */
            $calendarTimeZone = $this->calendarTimeZoneFactory->create(['calendar' => $calendar]);
            $this->calendarsTimeZones[$calendarId] = new \DateTimeZone($calendarTimeZone->getTimezone());
        }
        return $this->calendarsTimeZones[$calendarId];
    }
}
