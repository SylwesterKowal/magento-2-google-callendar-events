<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\DataProvider;

class TagsFormDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag
     */
    private $resource;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $resource
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Tag $resource,
        array $meta = [],
        array $data = []
    ) {
        $this->resource = $resource;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $this->data['items'][] = [
            'id_field_name' => 'fake_id',
            'fake_id' => null,
            'tags' => $this->getTags()
        ];

        return $this->data;
    }

    private function getTags()
    {
        $connection = $this->resource->getConnection();
        $select = $connection->select();
        $select
            ->from(['t' => $this->resource->getMainTable()], ['tag_title']);
        $tags = $connection->fetchCol($select);
        $tags = empty($tags) ? '' : implode(', ', $tags);

        return $tags;
    }

    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        return;
    }
}
