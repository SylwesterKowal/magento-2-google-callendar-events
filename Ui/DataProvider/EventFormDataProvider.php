<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Ui\DataProvider;

use Celesta\AdvancedGoogleCalendar\Model\ReminderHelper;

class EventFormDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var ReminderHelper
     */
    private $reminderHelper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory
     */
    protected $collection;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper
     */
    private $eventMapper;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\TagRepository
     */
    private $tagRepository;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventDateTime
     */
    private $eventDate;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory
     */
    private $calendarTimeZoneFactory;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDate
     * @param \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar
     * @param ReminderHelper $reminderHelper
     * @param \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository
     * @param \Celesta\AdvancedGoogleCalendar\Model\User $user
     * @param \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper
     * @param \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $eventsCollectionFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResourceConnection $resource,
        \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDate,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar,
        ReminderHelper $reminderHelper,
        \Celesta\AdvancedGoogleCalendar\Model\TagRepository $tagRepository,
        \Celesta\AdvancedGoogleCalendar\Model\User $user,
        \Celesta\AdvancedGoogleCalendar\Model\Block\EventValueMapper $eventMapper,
        \Celesta\AdvancedGoogleCalendar\Model\ResourceModel\Event\CollectionFactory $eventsCollectionFactory,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, 'event', $meta, $data);
        $this->request = $request;
        $this->resource = $resource;
        $this->googleCalendar = $googleCalendar;
        $this->reminderHelper = $reminderHelper;
        $this->eventMapper = $eventMapper;
        $this->collection = $eventsCollectionFactory->create();
        $this->user = $user;
        $this->tagRepository = $tagRepository;
        $this->eventDate = $eventDate;
        $this->calendarTimeZoneFactory = $calendarTimeZoneFactory;
    }

    public function getRequestFieldName()
    {
        return $this->requestFieldName;
    }

    /**
     * {@inheritdoc}
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getData()
    {
        $calendarId = $this->request->getParam('calendar');
        $eventId = $this->request->getParam('event');
        if ($this->isNewEvent($calendarId, $eventId)) {
            $this->data[null] = [
                'calendar_id_visible' => true,
                'calendar_summary_visible' => false,
                'is_recurrent_event' => false,
                'is_single_event' => true,
            ];
        } else {
            $calendar = $this->user->getUserCalendarById($calendarId);
            $event = $this->googleCalendar->getEventById($calendar->getGoogleId(), $eventId);
            /** @var \Google_Service_Calendar_EventAttendee[] $attendees */
            $emails = [];
            $attendees = $event->getAttendees();
            foreach ($attendees as $attendee) {
                $emails[] = $attendee->getDisplayName() ?
                    $attendee->getDisplayName() . ' <' . $attendee->getEmail() . '>' :
                    $attendee->getEmail();
            }
            $this->data[$event->getId()] = [
                $this->getPrimaryFieldName() => $event->getId(),
                'calendar_id' => $calendarId,
                'calendar_summary' => '<strong>' . $calendar->getSummary() . '<strong>',
                'calendar_id_visible' => false,
                'calendar_summary_visible' => true,
                'summary' => $event->getSummary(),
                'description' => $event->getDescription(),
                'location' => $event->getLocation(),
                'color_id' => $event->getColorId(),
                'visibility' => $this->eventMapper->getVisibilityKeyByGoogleValue($event->getVisibility()),
                'transparency' => $this->eventMapper->getTransparencyKeyByGoogleValue($event->getTransparency()),
                'recurrence' => (int)$event->getRecurrence(),
                'recurrence_readable' => $this->eventDate->getRecurrenceReadable($event),
                'is_recurrent_event' => (bool)$event->getRecurrence(),
                'is_single_event' => !$event->getRecurrence(),
                'tags' => $this->getTagsIds($event),
                'event_date_time' => $this->getEventDateTime($event, $calendar),
                'guests' => [
                    'guest_emails' => implode(', ', $emails),
                    'guests_modify_event' => $event->getGuestsCanModify() === true ? '1' : '0',
                    'guests_invite_others' => $event->getGuestsCanInviteOthers() === false ? '0' : '1',
                    'guests_see_guest_list' => $event->getGuestsCanSeeOtherGuests() === false ? '0' : '1',
                    'hangouts_link' => $event->getHangoutLink() === null ?
                        '<span style="color:#808080;">' . __('Video call not added') . '</span>' :
                        '<a href="' . $event->getHangoutLink() . '" target="_blank">' . __('Join video call') . '</a>',
                ],
                'reminders' => $this->getReminderSettings($event),
            ];
        }

        return $this->data;
    }

    private function getTagsIds(\Google_Service_Calendar_Event $event)
    {
        $extendedProperties = $event->getExtendedProperties();
        if ($extendedProperties) {
            $shared = $extendedProperties->getShared();
            if (isset($shared['tags'])) {
                return $this->tagRepository->getIdsByTitles(explode(',', $shared['tags']));
            }
        }

        return [];
    }

    /**
     * Be aware that timezone only matters for events with exact time, but not for all day events
     * @param \Google_Service_Calendar_Event $event
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @return array
     */
    private function getEventDateTime(
        \Google_Service_Calendar_Event $event,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
    ) {
        $eventStart = $event->getStart();
        $eventEnd = $event->getEnd();

        /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZone $calendarTimeZone */
        $calendarTimeZone = $this->calendarTimeZoneFactory->create(['calendar' => $calendar]);
        $timeZone = new \DateTimeZone($calendarTimeZone->getTimezone());

        if ($eventStart->getDateTime()) {
            $start = new \DateTime($eventStart->getDateTime());
            $start->setTimezone($timeZone);
            $start = $start->format('c');
        } else {
            $start = $eventStart->getDate();
        }

        if ($eventEnd->getDateTime()) {
            $end = new \DateTime($eventEnd->getDateTime());
            $end->setTimezone($timeZone);
            $end = $end->format('c');
        } else {
            $end = new \DateTime($eventEnd->getDate());
            $end = $end->modify('-1 days')->format('Y-m-d');
        }

        $data = [
            'date_from' => $start,
            'date_to' => $end,
            'time_zone' => $timeZone->getName(),
            'all_day' => $eventStart->getDateTime() ? '0' : '1'
        ];
        if ($eventStart->getDateTime()) {
            $data['time_from'] = $start;
            $data['time_to'] = $end;
        }
        return $data;
    }

    private function getReminderSettings(\Google_Service_Calendar_Event $event)
    {
        $reminders = [];
        /** @var \Google_Service_Calendar_EventReminder[] $calendarReminders */
        $calendarReminders = $event->getReminders()->getOverrides();
        foreach ($calendarReminders as $cr) {
            list($value, $unit) = $this->reminderHelper->getBeforeUnits($cr->getMinutes());
            $reminders[] = [
                'reminder_type' => $cr->getMethod(),
                'before_value' => $value,
                'before_unit' => $unit
            ];
        }

        $extendedProperties = $event->getExtendedProperties();
        if ($extendedProperties) {
            $shared = $extendedProperties->getShared();
            if (isset($shared[ReminderHelper::GOOGLE_CALENDAR_SYSTEM_KEY])) {
                list($value, $unit) = $this->reminderHelper->getBeforeUnits(
                    $shared[ReminderHelper::GOOGLE_CALENDAR_SYSTEM_KEY]
                );
                $reminders[] = [
                    'reminder_type' => ReminderHelper::TYPE_SYSTEM,
                    'before_value' => $value,
                    'before_unit' => $unit
                ];
            }
        }

        return ['reminders' => $reminders];
    }

    /**
     * @param string $calendarId
     * @param string $eventId
     * @return bool
     */
    private function isNewEvent($calendarId, $eventId)
    {
        return empty($calendarId) || empty($eventId) || $this->user->getUserCalendarById($calendarId, true) === null;
    }
}
