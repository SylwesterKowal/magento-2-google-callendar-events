<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Observer;

class UserGoogleSettings implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    private $authorization;

    public function __construct(\Magento\Framework\AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->authorization->isAllowed('Celesta_Celesta::celesta')) {
            $block = $observer->getEvent()->getBlock();
            if ($block instanceof \Magento\User\Block\User\Edit\Tabs) {
                $block->addTab('celesta_settings', [
                    'label' => 'Celesta',
                    'title' => 'Celesta',
                    'after' => 'roles_section',
                    'content' => $block->getLayout()->createBlock(
                        \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\User\GoogleSettings::class
                    )->toHtml()
                ]);
            }
        }
        return $this;
    }
}
