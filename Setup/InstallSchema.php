<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Setup;

use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    const CALENDAR_CALENDAR_TABLE_NAME = 'celesta_calendar_calendar';
    const CALENDAR_EVENT_TABLE_NAME = 'celesta_calendar_event';
    const CALENDAR_EVENT_ATTENDEE_TABLE_NAME = 'celesta_calendar_event_attendee';
    const CALENDAR_TAG_TABLE_NAME = 'celesta_calendar_tag';
    const CALENDAR_TAG_PIVOT_TABLE_NAME = 'celesta_calendar_tag_pivot';
    const CALENDAR_REMINDER_TABLE_NAME = 'celesta_calendar_reminder';

    /**
     * {@inheritdoc}
     * @throws \Zend_Db_Exception
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'celesta_calendar_calendar'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_CALENDAR_TABLE_NAME))
            ->addColumn(
                'calendar_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                    'auto_increment' => true
                ]
            )
            ->addColumn('calendar_google_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('user_id', Table::TYPE_INTEGER, null, ['unsigned' => true])
            ->addColumn('summary', Table::TYPE_TEXT, 255)
            ->addColumn('time_zone', Table::TYPE_TEXT, 100)
            ->addColumn('use_system_time_zone', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('is_primary', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('is_connected', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('is_dashboard', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('share_reminders', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('description', Table::TYPE_TEXT, '64k')
            ->addColumn('etag', Table::TYPE_TEXT, 255)
            ->addIndex($installer->getIdxName(self::CALENDAR_CALENDAR_TABLE_NAME, 'calendar_id'), 'calendar_id')
            ->addIndex($installer->getIdxName(self::CALENDAR_CALENDAR_TABLE_NAME, 'user_id'), 'user_id');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'celesta_calendar_event'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_EVENT_TABLE_NAME))
            ->addColumn('event_hash', Table::TYPE_TEXT, 32, ['nullable' => false, 'primary' => true])
            ->addColumn('event_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('calendar_id', Table::TYPE_INTEGER, null, ['unsigned' => true])
            ->addColumn('summary', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('description', Table::TYPE_TEXT, '64k', ['nullable' => true])
            ->addColumn('location', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('color_id', Table::TYPE_SMALLINT, 2, ['unsigned' => true])
            ->addColumn('recurrence', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('transparency', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('visibility', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('all_day', Table::TYPE_BOOLEAN, 1, ['unsigned' => true])
            ->addColumn('start_utc', Table::TYPE_DATETIME, null, ['nullable' => false])
            ->addColumn('end_utc', Table::TYPE_DATETIME, null, ['nullable' => false])
            ->addColumn('html_link', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addIndex($installer->getIdxName(self::CALENDAR_EVENT_TABLE_NAME, 'event_hash'), 'event_hash')
            ->addIndex($installer->getIdxName(self::CALENDAR_EVENT_TABLE_NAME, 'event_id'), 'event_id')
            ->addIndex($installer->getIdxName(self::CALENDAR_EVENT_TABLE_NAME, 'calendar_id'), 'calendar_id')
            ->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_EVENT_TABLE_NAME,
                    'calendar_id',
                    self::CALENDAR_CALENDAR_TABLE_NAME,
                    'calendar_id'
                ),
                'calendar_id',
                $installer->getTable(self::CALENDAR_CALENDAR_TABLE_NAME),
                'calendar_id',
                Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'celesta_calendar_event_attendee'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_EVENT_ATTENDEE_TABLE_NAME))
            ->addColumn('event_attendee_hash', Table::TYPE_TEXT, 32, ['nullable' => false, 'primary' => true])
            ->addColumn('event_hash', Table::TYPE_TEXT, 32, ['nullable' => false])
            ->addColumn('event_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('calendar_google_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('creator_email', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('attendee_email', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('attendee_display_name', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('response_status', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('additional_guests', Table::TYPE_SMALLINT, 3, ['unsigned' => true, 'nullable' => true])
            ->addColumn('comment', Table::TYPE_TEXT, 255, ['nullable' => true])
            ->addColumn('addressee_status', Table::TYPE_BOOLEAN, 1)
            ->addIndex(
                $installer->getIdxName(self::CALENDAR_EVENT_ATTENDEE_TABLE_NAME, 'event_attendee_hash'),
                'event_attendee_hash'
            )
            ->addIndex($installer->getIdxName(self::CALENDAR_EVENT_ATTENDEE_TABLE_NAME, 'event_hash'), 'event_hash')
            ->addIndex(
                $installer->getIdxName(self::CALENDAR_EVENT_ATTENDEE_TABLE_NAME, 'attendee_email'),
                'attendee_email'
            )
            ->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_EVENT_ATTENDEE_TABLE_NAME,
                    'event_hash',
                    self::CALENDAR_EVENT_TABLE_NAME,
                    'event_hash'
                ),
                'event_hash',
                $installer->getTable(self::CALENDAR_EVENT_TABLE_NAME),
                'event_hash',
                Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'celesta_calendar_reminder'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_REMINDER_TABLE_NAME))
            ->addColumn('reminder_hash', Table::TYPE_TEXT, 32, ['nullable' => false, 'primary' => true])
            ->addColumn('event_id', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addColumn('calendar_id', Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false])
            ->addColumn('start', Table::TYPE_TIMESTAMP, null, ['nullable' => false])
            ->addColumn('end', Table::TYPE_TIMESTAMP, null, ['nullable' => false])
            ->addIndex($installer->getIdxName(self::CALENDAR_REMINDER_TABLE_NAME, 'event_id'), 'event_id')
            ->addIndex($installer->getIdxName(self::CALENDAR_CALENDAR_TABLE_NAME, 'calendar_id'), 'calendar_id')
            /**
             * @todo should it be event_id or event_hash ?
             */
            ->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_REMINDER_TABLE_NAME,
                    'event_id',
                    self::CALENDAR_EVENT_TABLE_NAME,
                    'event_id'
                ),
                'event_id',
                $installer->getTable(self::CALENDAR_EVENT_TABLE_NAME),
                'event_id',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_REMINDER_TABLE_NAME,
                    'calendar_id',
                    self::CALENDAR_CALENDAR_TABLE_NAME,
                    'calendar_id'
                ),
                'calendar_id',
                $installer->getTable(self::CALENDAR_CALENDAR_TABLE_NAME),
                'calendar_id',
                Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'celesta_calendar_tag'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_TAG_TABLE_NAME))
            ->addColumn(
                'tag_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                    'auto_increment' => true
                ]
            )
            ->addColumn('tag_title', Table::TYPE_TEXT, 255, ['nullable' => false])
            ->addIndex($installer->getIdxName(self::CALENDAR_TAG_TABLE_NAME, 'tag_id'), 'tag_id')
            ->addIndex($installer->getIdxName(self::CALENDAR_TAG_TABLE_NAME, 'tag_title'), 'tag_title');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'celesta_calendar_tag_pivot'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable(self::CALENDAR_TAG_PIVOT_TABLE_NAME))
            ->addColumn('event_hash', Table::TYPE_TEXT, 32, ['nullable' => false])
            ->addColumn('tag_id', Table::TYPE_INTEGER, null, ['nullable' => false, 'unsigned' => true])
            ->addIndex($installer->getIdxName(self::CALENDAR_TAG_PIVOT_TABLE_NAME, 'event_hash'), 'event_hash')
            ->addIndex($installer->getIdxName(self::CALENDAR_TAG_PIVOT_TABLE_NAME, 'tag_id'), 'tag_id')
            ->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_TAG_PIVOT_TABLE_NAME,
                    'event_hash',
                    self::CALENDAR_EVENT_TABLE_NAME,
                    'event_hash'
                ),
                'event_hash',
                $installer->getTable(self::CALENDAR_EVENT_TABLE_NAME),
                'event_hash',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    self::CALENDAR_TAG_PIVOT_TABLE_NAME,
                    'tag_id',
                    self::CALENDAR_TAG_TABLE_NAME,
                    'tag_id'
                ),
                'tag_id',
                $installer->getTable(self::CALENDAR_TAG_TABLE_NAME),
                'tag_id',
                Table::ACTION_CASCADE
            );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
