###Thank you!
Thank you for choosing Advanced Google Calendar! We hope you'll enjoy using our extension and make your store administration more productive.


###Installation
To install the extension follow the instructions below:
1. Backup your web directory and store database
2. Get the FAQ name and version in your Marketplace account:
    - Login to Magento Marketplace with the username and password you used to purchase the FAQ
extension;
    - Enter the My Purchases section: **Your name (top-right corner of the page) > My Profile > My
Purchases**;
    - Find FAQ and click **Technical details**.
3. Copy the provided component name and version in order to update your composer.json file.

    **Note:** We advise you to use the latest version of our extension.
4. Update your composer.json file. Open the root Magento directory on your server and send Composer the following command: ```composer require <faq>:<1.0.0>```

5. At the Composer request, enter your Magento marketplace credentials (public key - username,
private key - password).
6.  Make sure that Composer finished the installation without errors.
7.  Flush store cache, log out and log in the backend again.
FAQ extension is now installed and ready for work.
Use Magento Dev Docs for more details.
