<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml;

class EventDescription extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventDateTime
     */
    private $eventDateTime;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory
     */
    private $calendarTimeZoneFactory;

    /**
     * @var \Google_Service_Calendar_Event
     */
    private $event;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Calendar
     */
    private $calendar;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime
     * @param \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param \Google_Service_Calendar_Event $event
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Celesta\AdvancedGoogleCalendar\Model\EventDateTime $eventDateTime,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar,
        \Google_Service_Calendar_Event $event,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
        $this->eventDateTime = $eventDateTime;
        $this->calendarTimeZoneFactory = $calendarTimeZoneFactory;
        $this->calendar = $calendar;
        $this->event = $event;
    }

    public function _toHtml()
    {
        $event = $this->event;
        /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZone $calendarTimeZone */
        $calendarTimeZone = $this->calendarTimeZoneFactory->create(['calendar' => $this->calendar]);
        $timeZone = new \DateTimeZone($calendarTimeZone->getTimezone());

        $items[] = '<tr><th colspan="2"><a href="' . $event->getHtmlLink() . '" target="_blank">' .
            ($event->getSummary() ? $event->getSummary() : __('No title')) .
            '</a></th></tr>';
        $dateValue = $this->eventDateTime->getEventDateAndTimeReadable($event, $timeZone);
        $items[] = '<tr><td>' . __('When') . '</td><td>' . $dateValue . '</td></tr>';

        if ($event->getLocation()) {
            $items[] = '<tr><td>' . __('Where') . '</td><td>' . $event->getLocation() . '</td></tr>';
        }
        if ($event->getDescription()) {
            $items[] = '<tr><td style="padding-right:15px;vertical-align:top;">' .
                __('Description') . '</td><td>' . $event->getDescription() . '</td></tr>';
        }
        $calendarSummary = $this->calendar->getSummary();
        $calendarSummary = $calendarSummary ?: $event->getOrganizer()->getEmail();
        if ($calendarSummary) {
            $items[] = '<tr><td>' . __('Calendar') . '</td><td>' . $calendarSummary . '</td></tr>';
        }
        $creator = $event->getCreator();
        $items[] = '<tr><td>' . __('Created by') .'</td><td>'.
            '<a href="mailto:' . $creator->getEmail() . '" target="_blank">' .
            ($creator->getDisplayName() ?: $event->getCreator()->getEmail()) .
            '</a></td></tr>';

        if ($event->getAttendees()) {
            $attendees = array_map(function ($a) {
                return '<a href="mailto:' . $a->getEmail() . '">' . ($a->getDisplayName() ?: $a->getEmail()) . '</a>';
            }, $event->getAttendees());
            $items[] = '<tr><td>' .
                __('Attendees') . '</td><td>+' . count($attendees) .
                ' (<a href="#" onclick="this.style.display=\'none\';this.nextSibling.style.display=\'inline\';return false;">'.__('see all').'&hellip;</a><span style="display:none;">'.implode(', ', $attendees).
                '</span>)</td></tr>';
        }
        $html = '<table class="celesta_event_description_table">' . implode('', $items) . '</table>';

        $apiKey = $this->getGoogleApiKey();
        if ($apiKey && $event->getLocation()) {
            $html = '<iframe width="400" height="250" frameborder="0" style="border:0;margin-right:15px;" ' .
                'src="https://www.google.com/maps/embed/v1/place?key=' . $apiKey . '&q=' . $event->getLocation() .
                '" allowfullscreen></iframe>' . $html;
        }

        return $html;
    }

    private function getGoogleApiKey()
    {
        return $this->scopeConfig->getValue(\Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH_GOOGLE_API_KEY);
    }
}
