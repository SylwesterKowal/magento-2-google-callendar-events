<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml\User;

class GoogleSettings extends \Magento\Backend\Block\Widget\Form
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Data\FormFactory
     */
    private $formFactory;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $calendarFactory;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $userFactory;

    public function __construct(
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendarFactory $calendarFactory,
        \Celesta\AdvancedGoogleCalendar\Model\UserFactory $userFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->url = $url;
        $this->coreRegistry = $coreRegistry;
        $this->scopeConfig = $scopeConfig;
        $this->formFactory = $formFactory;
        $this->calendarFactory = $calendarFactory;
        $this->userFactory = $userFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return \Magento\Backend\Block\Widget\Form
     * @throws \Exception
     * @see \Magento\User\Block\User\Edit\Tab\Main
     */
    protected function _prepareForm()
    {
        $userId = $this->coreRegistry->registry('permissions_user')->getId();
        /** @var \Celesta\AdvancedGoogleCalendar\Model\User $calendarUser */
        $calendarUser = $this->userFactory->create(['userId' => $userId]);
        $clientId = $calendarUser->getClientId();
        $clientSecret = $calendarUser->getClientSecret();

        /** @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $calendar */
        $calendar = $this->calendarFactory->create(['user' => $calendarUser]);
        try {
            $accessToken = $calendar->getClient()->getAccessToken();
            $accessToken = $accessToken['access_token'];
        } catch (\Celesta\AdvancedGoogleCalendar\Model\Exception $e) {
            $accessToken = null;
        }
        if ($calendarUser->isAccessTokenValid($accessToken) === false) {
            $accessToken = null;
            $calendarUser->dropAccessToken();
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->formFactory->create();
        $googleFieldset = $form->addFieldset('google_fieldset', ['legend' => __('Google API authentication')]);

        if ($userId === null || !$calendarUser->isSessionUser()) {
            $name = $this->coreRegistry->registry('permissions_user')->getName();
            $username = $this->coreRegistry->registry('permissions_user')->getUserName();
            $html =
                '<div style="margin-bottom: .5rem;">' .
                '<strong>Connection should be set up by user personally</strong>' .
                '</div>' .
                '<div style="line-height: 1.5;">' .
                'To manage Google API authentication you should be logged in to Magento Admin Panel as ' .
                ($userId ? '<strong>' . $username . ' (' . $name . ').' . '</strong>' : 'newly created user.') .
                '</div>';
            $googleFieldset->addField('google_comment', \Celesta\AdvancedGoogleCalendar\Ui\Element\Custom::class, ['html' => $html]);
        } elseif ($clientId && $clientSecret && $accessToken) {
            $html = '<div style="margin-bottom: .5rem;">' .
                '<strong>User is authenticated</strong>' .
                '</div>' .
                '<div style="line-height: 1.5;">' .
                'Magento user is authenticated in Google. Now you can grant access credentials to Celesta extensions. Please check authorization requests in Admin System Messages.' .
                '<br>You can also drop your Google authentication data by clicking ' .
                '<em>Drop data</em> button below.' .
                '</div>' .
                '<br><button onclick="javascript:document.location.href=\'' .
                $this->url->getUrl(\Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'event/dropAuth', ['user_id' => $userId]) .
                '\'; return false;">Drop data</button>';
            $googleFieldset->addField('google_comment', \Celesta\AdvancedGoogleCalendar\Ui\Element\Custom::class, ['html' => $html]);
        } elseif ($clientId && $clientSecret) {
            $html = '<div style="margin-bottom: .5rem;">' .
                '<strong>Authentication data saved</strong>' .
                '</div>' .
                '<div style="line-height: 1.5;">' .
                'Authentication data for Google services saved. Now you can grant access credentials to Celesta ' .
                'extensions. Please check authorization requests in Admin System Messages.<br>You can also drop your Google authentication data by clicking ' .
                '<em>Drop data</em> button below.' .
                '</div>' .
                '<br><button onclick="javascript:document.location.href=\'' .
                $this->url->getUrl(\Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'event/dropAuth', ['user_id' => $userId]) .
                '\'; return false;">Drop data</button>';
            $googleFieldset->addField(
                'google_comment',
                \Celesta\AdvancedGoogleCalendar\Ui\Element\Custom::class,
                ['html' => $html]
            );
        } else {
            $googleFieldset->addField('google_client_id', 'text', [
                'name' => 'client_id',
                'label' => __('Client ID'),
                'id' => 'google_client_id',
                'title' => __('Client ID'),
                'required' => false
            ]);
            $googleFieldset->addField('google_client_secret', 'text', [
                'name' => 'client_secret',
                'label' => __('Client Secret'),
                'id' => 'google_client_secret',
                'title' => __('Client Secret'),
                'required' => false
            ]);
        }
        if (!$this->scopeConfig->getValue(\Celesta\AdvancedGoogleCalendar\Model\Module::CONFIG_PATH_GOOGLE_API_KEY)) {
            $apiKeyUrl = $this->url->getUrl('adminhtml/system_config/edit', ['section' => 'celesta_google_api']);
            $html = '<div style="margin-bottom: .5rem;">' .
                '<strong>Add Google Api Key</strong>' .
                '</div>' .
                '<div style="line-height: 1.5;">' .
                'You can also add Google Api Key to make requests to Google services like Google Maps for displaying map widget for example. Google Api Key is not specific to the user but to the application so you can configure it in <a target="_blank" href="' . $apiKeyUrl . '">System Configuration</a>.' .
                '</div>';
            $googleFieldset->addField(
                'google_api_key_comment',
                \Celesta\AdvancedGoogleCalendar\Ui\Element\Custom::class,
                ['html' => $html]
            );
        }

        $values = [];
        if ($clientId) {
            $values['google_client_id'] = $clientId;
        }
        if ($clientSecret) {
            $values['google_client_secret'] = $clientSecret;
        }
        if ($values) {
            $form->setValues($values);
        }
        $this->setForm($form);

        return $this;
    }
}
