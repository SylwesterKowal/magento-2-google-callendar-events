<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml\Buttons;

class PrintEvent implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar
     */
    private $googleCalendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\User
     */
    private $user;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Celesta\AdvancedGoogleCalendar\Model\GoogleCalendar $googleCalendar,
        \Celesta\AdvancedGoogleCalendar\Model\User $user
    ) {
        $this->request = $request;
        $this->googleCalendar = $googleCalendar;
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $calendarId = $this->request->getParam('calendar');
        $eventId = $this->request->getParam('event');
        if (!$this->isNewEvent($calendarId, $eventId)) {
            $calendar = $this->user->getUserCalendarById($calendarId);
            $event = $this->googleCalendar->getEventById($calendar->getGoogleId(), $eventId);
            if ($event) {
                $parts = parse_url($event->getHtmlLink());
                parse_str($parts['query'], $params);

                return [
                    'label' => __('Print'),
                    'on_click' => sprintf(
                        "window.open('%s','_blank').focus();return false;",
                        sprintf(
                            'https://calendar.google.com/calendar/printevent?eid=%s&sf=true&pjs=true',
                            $params['eid']
                        )
                    )
                ];
            }
        }

        return [];
    }

    /**
     * @param string $calendarId
     * @param string $eventId
     * @return bool
     */
    private function isNewEvent($calendarId, $eventId)
    {
        return empty($calendarId) || empty($eventId) || $this->user->getUserCalendarById($calendarId, true) === null;
    }
}
