<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml\Buttons;

class Save implements \Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save'),
            'class_name' => \Magento\Backend\Block\Widget\Button\SplitButton::class,
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save'
            ],
            'options' => $this->getOptions()
        ];
    }

    /**
     * @return array
     */
    private function getOptions()
    {
        return [[
            'label' => __('Save & New'),
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'celesta_calendar_event_edit_form.areas',
                                'actionName' => 'redirectTo',
                                'params' => ['new']
                            ]
                        ]
                    ]
                ]
            ]
        ], [
            'label' => __('Save & Close'),
            'data_attribute' => [
                'mage-init' => [
                    'buttonAdapter' => [
                        'actions' => [
                            [
                                'targetName' => 'celesta_calendar_event_edit_form.areas',
                                'actionName' => 'redirectTo',
                                'params' => ['close']
                            ]
                        ]
                    ]
                ]
            ]
        ]];
    }
}
