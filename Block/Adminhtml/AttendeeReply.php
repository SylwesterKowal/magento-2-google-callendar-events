<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml;

class AttendeeReply extends \Magento\Framework\View\Element\Template
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'attendee_reply.phtml';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface
     */
    private $dateTimeFormatter;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee[]
     */
    private $attendees;

    /**
     * @var \Google_Service_Calendar_Event
     */
    private $event;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Calendar
     */
    private $calendar;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface $dateTimeFormatter
     * @param \Google_Service_Calendar_Event $event
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendee[] $attendees
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTimeFormatterInterface $dateTimeFormatter,
        \Google_Service_Calendar_Event $event,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar,
        array $attendees,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->dateTimeFormatter = $dateTimeFormatter;
        $this->event = $event;
        $this->calendar = $calendar;
        $this->attendees = $attendees;
    }

    public function getAttendees()
    {
        return $this->attendees;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function getFormUrl()
    {
        return parent::getUrl(
            \Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'attendee/replyConfirmRead'
        );
    }

    public function getEventAttendeeHashes()
    {
        $h = array_map(function ($a) {
            /** @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $a */
            return $a->getId();
        }, $this->attendees);

        return $h;
    }

    public function getCurrentReplies()
    {
        $attendees = array_map(function ($a) {
            /** @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $a */
            return '<a href="mailto:' . $a->getAttendeeEmail() . '">' . ($a->getAttendeeDisplayName() ?: $a->getAttendeeEmail()) . '</a>';
        }, $this->attendees);
        return __(
            '%1+%2 more attendee(s)%3 replied to your invitation',
            '<a href="#" style="display:inline;" onclick="this.style.display=\'none\';this.nextSibling.style.display=\'inline\';return false;">',
            count($attendees),
            '</a><span style="display:none;">' . implode(', ', $attendees) . '</span>'
        );
    }

    public function getAttendeesSummary()
    {
        $guests = 0;
        $grouped = [];
        /** @var \Google_Service_Calendar_EventAttendee[] $attendees */
        $attendees = $this->event->getAttendees();
        foreach ($attendees as $attendee) {
            $grouped[$attendee->getResponseStatus()][] = $attendee;
            if ($attendee->getAdditionalGuests()) {
                $guests += $attendee->getAdditionalGuests();
            }
        }
        $contents = [];
        $getInflated = function ($attendees) {
            return implode(', ', array_map(function ($a) {
                /** @var \Google_Service_Calendar_EventAttendee $a */
                return '<a href="mailto:' . $a->getEmail() . '">' . ($a->getDisplayName() ?: $a->getEmail()) . '</a>';
            }, $attendees));
        };

        $contents[] = '<tr><th colspan="2">' . __('Attendance Summary') . '</th></tr>';
        $contents[] = '<tr><td colspan="2">' . $this->getCurrentReplies() . '</td></tr>';
        if ($guests) {
            $contents[] = sprintf('<tr><td>%s:</td><td>+%d</td></tr>', __('Additional guests'), $guests);
        }
        if ($grouped) {
            foreach ($grouped as $status => $group) {
                $c = ['needsAction' => '', 'accepted' => 'green', 'tentative' => 'gray', 'declined' => 'red'];
                $t = ['needsAction' => 'Awaiting for reply', 'accepted' => 'Yes, I\'m going', 'tentative' => 'I might go', 'declined' => 'No, I\'m not going'];
                $contents[$status] = sprintf(
                    '<tr><td class="celesta_%s">%s:</td><td>%d %s %d (<a href="#" onclick="this.style.display=\'none\';this.nextSibling.style.display=\'inline\';return false;">%s&hellip;</a><span style="display:none;">%s</span>)</td></tr>',
                    $c[$status],
                    __($t[$status]),
                    count($group),
                    __('of'),
                    count($attendees),
                    __('see all'),
                    $getInflated($group)
                );
            }
        }

        return '<table class="celesta_attendees_summary_table">' . implode('', $contents) . '</table>';
    }

    public function getEventContent()
    {
        $attendee = reset($this->attendees);

        return $this->getLayout()->createBlock(
            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\EventDescription::class,
            'celesta_notification event_block' .
            md5($this->event->getOrganizer()->getEmail() . $this->event->getId() . self::class),
            [
                'event' => $this->event,
                'calendar' => $this->calendar,
                'data' => ['calendar_summary' => $attendee->getData('calendar_summary')]
            ]
        )->toHtml();
    }
}
