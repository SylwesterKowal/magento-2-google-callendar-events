<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml;

class AttendeeAction extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'attendee_action.phtml';

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\EventAttendee
     */
    private $attendee;

    /**
     * @var \Google_Service_Calendar_Event
     */
    private $event;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Calendar
     */
    private $calendar;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Google_Service_Calendar_Event $event
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $attendee
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Google_Service_Calendar_Event $event,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar,
        \Celesta\AdvancedGoogleCalendar\Model\EventAttendee $attendee,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->event = $event;
        $this->calendar = $calendar;
        $this->attendee = $attendee;
    }

    public function getAttendee()
    {
        return $this->attendee;
    }

    public function getEvent()
    {
        return $this->event;
    }

    public function getFormUrl()
    {
        return parent::getUrl(
            \Celesta\AdvancedGoogleCalendar\Model\Module::ADMIN_AREA_PATH . 'attendee/action'
        );
    }

    public function getEventContent()
    {
        return $this->getLayout()->createBlock(
            \Celesta\AdvancedGoogleCalendar\Block\Adminhtml\EventDescription::class,
            'celesta_notification event_block' .
            md5($this->event->getOrganizer()->getEmail() . $this->event->getId() . self::class),
            [
                'event' => $this->event,
                'calendar' => $this->calendar,
                'data' => ['calendar_summary' => $this->attendee->getData('calendar_summary')]
            ]
        )->toHtml();
    }
}
