<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Block\Adminhtml\Dashboard;

class Calendar extends \Magento\Framework\View\Element\AbstractBlock
{
    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\Calendar
     */
    private $calendar;

    /**
     * @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory
     */
    private $calendarTimeZoneFactory;

    /**
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Context $context,
        \Celesta\AdvancedGoogleCalendar\Model\Calendar $calendar,
        \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZoneFactory $calendarTimeZoneFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->calendar = $calendar;
        $this->calendarTimeZoneFactory = $calendarTimeZoneFactory;
    }

    /**
     * @return string
     * @see https://fullcalendar.io/ to implement
     */
    public function toHtml()
    {
        /** @var \Celesta\AdvancedGoogleCalendar\Model\CalendarTimeZone $calendarTimeZone */
        $calendarTimeZone = $this->calendarTimeZoneFactory->create(['calendar' => $this->calendar]);
        return '<iframe src="https://calendar.google.com/calendar/embed?src=' . $this->calendar->getGoogleId() .
            '&ctz=' . $calendarTimeZone->getTimezone() . '" style="border: 0; width: 100%; height: 600px;" frameborder="0" scrolling="no"></iframe>';
    }
}
