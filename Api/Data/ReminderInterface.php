<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Api\Data;

interface ReminderInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getEventId();

    /**
     * @param string $eventId
     * @return $this
     */
    public function setEventId($eventId);

    /**
     * @return int
     */
    public function getCalendarId();

    /**
     * @param int $calendarId
     * @return $this
     */
    public function setCalendarId($calendarId);

    /**
     * @return string|\DateTimeInterface
     */
    public function getStart();

    /**
     * @param string|\DateTimeInterface $start
     * @return $this
     */
    public function setStart($start);

    /**
     * @return string|\DateTimeInterface
     */
    public function getEnd();

    /**
     * @param string|\DateTimeInterface $end
     * @return $this
     */
    public function setEnd($end);
}
