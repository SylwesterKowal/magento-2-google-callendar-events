<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Api\Data;

interface EventInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getEventId();

    /**
     * @param string $id
     * @return $this
     */
    public function setEventId($id);

    /**
     * @return int
     */
    public function getCalendarId();

    /**
     * @param int $calendarId
     * @return $this
     */
    public function setCalendarId($calendarId);

    /**
     * @return string
     */
    public function getSummary();

    /**
     * @param string $summary
     * @return $this
     */
    public function setSummary($summary);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getLocation();

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation($location);

    /**
     * @return int
     */
    public function getColorId();

    /**
     * @param int $colorId
     * @return $this
     */
    public function setColorId($colorId);

    /**
     * @return int
     */
    public function getRecurrence();

    /**
     * @param int $recurrence
     * @return $this
     */
    public function setRecurrence($recurrence);

    /**
     * @return int
     */
    public function getTransparency();

    /**
     * @param int $transparency
     * @return $this
     */
    public function setTransparency($transparency);

    /**
     * @return int
     */
    public function getVisibility();

    /**
     * @param int $visibility
     * @return $this
     */
    public function setVisibility($visibility);

    /**
     * @return string
     */
    public function getHtmlLink();

    /**
     * @param string $htmlLink
     * @return $this
     */
    public function setHtmlLink($htmlLink);
}
