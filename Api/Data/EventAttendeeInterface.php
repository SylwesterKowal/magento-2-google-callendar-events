<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Api\Data;

interface EventAttendeeInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @param string $eventAttendeeHash
     * @return $this
     */
    public function setId($eventAttendeeHash);

    /**
     * @return string
     */
    public function getEventHash();

    /**
     * @param string $eventHash
     * @return $this
     */
    public function setEventHash($eventHash);

    /**
     * @return string
     */
    public function getEventId();

    /**
     * @param string $eventId
     * @return $this
     */
    public function setEventId($eventId);

    /**
     * @return string
     */
    public function getCalendarGoogleId();

    /**
     * @param string $calendarId
     * @return $this
     */
    public function setCalendarGoogleId($calendarId);

    /**
     * @return string
     */
    public function getCreatorEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setCreatorEmail($email);

    /**
     * @return string
     */
    public function getAttendeeEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setAttendeeEmail($email);

    /**
     * @return string
     */
    public function getAttendeeDisplayName();

    /**
     * @param string $displayName
     * @return $this
     */
    public function setAttendeeDisplayName($displayName);

    /**
     * @return string
     */
    public function getResponseStatus();

    /**
     * @param string $responseStatus
     * @return $this
     */
    public function setResponseStatus($responseStatus);

    /**
     * @return int
     */
    public function getAdditionalGuests();

    /**
     * @param int $additionalGuests
     * @return $this
     */
    public function setAdditionalGuests($additionalGuests);

    /**
     * @return string
     */
    public function getComment();

    /**
     * @param string $comment
     * @return $this
     */
    public function setComment($comment);

    /**
     * @return int
     */
    public function getAddresseeStatus();

    /**
     * @param int $status
     * @return $this
     */
    public function setAddresseeStatus($status);
}
