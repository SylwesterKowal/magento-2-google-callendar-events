<?php
/**
 * @copyright Copyright (c) 2018 Yurii Hryhoriev aka Celesta
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Celesta\AdvancedGoogleCalendar\Api\Data;

interface CalendarInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getGoogleId();

    /**
     * @param string $googleId
     * @return $this
     */
    public function setGoogleId($googleId);

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * @return string
     */
    public function getSummary();

    /**
     * @param string $summary
     * @return $this
     */
    public function setSummary($summary);

    /**
     * @return string
     */
    public function getTimeZone();

    /**
     * @param string $timeZone
     * @return $this
     */
    public function setTimeZone($timeZone);

    /**
     * @return int
     */
    public function getUseSystemTimeZone();

    /**
     * @param int $useSystemTimeZone
     * @return $this
     */
    public function setUseSystemTimeZone($useSystemTimeZone);

    /**
     * @return int
     */
    public function getIsPrimary();

    /**
     * @param int $isPrimary
     * @return $this
     */
    public function setIsPrimary($isPrimary);

    /**
     * @return int
     */
    public function getIsConnected();

    /**
     * @param int $isConnected
     * @return $this
     */
    public function setIsConnected($isConnected);

    /**
     * @return int
     */
    public function getIsDashboard();

    /**
     * @param int $isDashboard
     * @return $this
     */
    public function setIsDashboard($isDashboard);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getEtag();

    /**
     * @param string $etag
     * @return $this
     */
    public function setEtag($etag);
}
